



typedef enum
{
    set_default = 0,
    set_value = 1,
    get_value = 2,
    not_set = 4
}actions_t;

typedef enum
{
    ble = 0,
    uart = 1,
    na = 2
}interface_t;


void console_process_char( char c );
void process_command( interface_t interface );
void console_insert_char( char c );
int new_hex_str_to_int( const char* hex_str );
void process_action(int property, char *parameters);

void check_for_set_cert_command( interface_t interface );
