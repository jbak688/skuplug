/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of
 * Cypress Semiconductor Corporation. All Rights Reserved.
 *
 * This software, associated documentation and materials ("Software"),
 * is owned by Cypress Semiconductor Corporation
 * or one of its subsidiaries ("Cypress") and is protected by and subject to
 * worldwide patent protection (United States and foreign),
 * United States copyright laws and international treaty provisions.
 * Therefore, you may use this Software only as provided in the license
 * agreement accompanying the software package from which you
 * obtained this Software ("EULA").
 * If no EULA applies, Cypress hereby grants you a personal, non-exclusive,
 * non-transferable license to copy, modify, and compile the Software
 * source code solely for use in connection with Cypress's
 * integrated circuit products. Any reproduction, modification, translation,
 * compilation, or representation of this Software except as specified
 * above is prohibited without the express written permission of Cypress.
 *
 * Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. Cypress
 * reserves the right to make changes to the Software without notice. Cypress
 * does not assume any liability arising out of the application or use of the
 * Software or any product or circuit described in the Software. Cypress does
 * not authorize its products for use in any products where a malfunction or
 * failure of the Cypress product may reasonably be expected to result in
 * significant property damage, injury or death ("High Risk Product"). By
 * including Cypress's product in a High Risk Product, the manufacturer
 * of such system or application assumes all risk of such use and in doing
 * so agrees to indemnify Cypress against all liability.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "wiced.h"
#include "wiced_utilities.h"
#include "ble_wifi_introducer.h"
#include "profab_command_interface.h"
#include "wiced_log.h"
#include "skyplug_config_dct.h"
#include "wiced_framework.h"

/******************************************************
 *                    Constants
 ******************************************************/
/*  Command Interface Property ID Definitions  */
#define DEVICE_ID                   0x01
#define FIRMWARE_VERSION            0x02
#define API_VERSION                 0x03
#define DEVICE_KEY                  0x04
#define MQTT_ENDPOINT               0x05
#define MQTT_PEM_CERTIFICATE        0x06
#define MQTT_PUBLIC_KEY             0x07
#define MQTT_PRIVATE_KEY            0x08
#define WIFI_SSID                   0x09
#define WIFI_AUTH_TYPE              0x0a
#define WIFI_USERNAME               0x0b
#define WIFI_PASSWORD               0x0c
#define SCANNED_SSIDS               0x0d
#define HAS_FAN                     0x0e
#define FAN_SPEEDS                  0x0f
#define IS_FAN_REVERSIBLE           0x10
#define LIGHT_MIN_DIM_LEVEL         0x11
#define LIGHT_TRANSITION_TIME       0x12
#define IS_LIGHT_DIMMABLE           0x13

/*  Command Interface Status ID Definitions  */
#define WIFI_STATUS                 0x80
#define LIGHT_POWER                 0x81
#define LIGHT_LEVEL                 0x82
#define FAN_POWER                   0x83
#define FAN_SPEED                   0x84
#define FAN_DIRECTION               0x85

/*  Command Interface Verb Definitions  */
#define SET_TO_DEFAULT              0x00
#define SET_VALUE                   0x01
#define GET_VALUE                   0x02

/******************************************************************************
 *                                Structures
 ******************************************************************************/
typedef struct
{

}property_defaults_t;

/******************************************************************************
 *                                Variables Definitions
 ******************************************************************************/
/*  Buffer used for recieved UART characters */
char                                        console_buffer[4096];//can we process entire cert in one command?
static uint32_t                                    console_cursor_position = 0;
static int                                         parameter_length;//using for debugging commands, remove
static platform_dct_security_t                     security_dct;

/*  Used to indicate when we are receiving a cert.  During receive, ignore new line and line feed characters*/
static wiced_bool_t                                receiving_cert;

/*  Being used for debugging print out current command buffer */
char command[10];

/* */
actions_t current_action = not_set;
interface_t interface_command_recieved_on = na;

/******************************************************************************
 *                                External Variables
 ******************************************************************************/
extern uint8_t skyplug_fan_level;
extern uint8_t skyplug_fan_power;
extern uint8_t skyplug_fan_direction;
extern uint8_t skyplug_light_level;
extern uint8_t skyplug_light_power;


void console_process_char( char c )
{

    switch ( c )
    {
        case 9: /* tab char */
                if (receiving_cert == TRUE){
                    console_insert_char( c );
                }
                break;

        case 10: /* line feed */
                /* ignore it */
            WPRINT_APP_INFO(("Line Feed character \r\n"));
            if (receiving_cert == TRUE)
            {
                console_insert_char( c );
            }
            break;

        case '\r': /* newline */

            if (receiving_cert == TRUE)
            {
                console_insert_char( c );
            }
            else
            {
                WPRINT_APP_INFO(("New Line character \r\n"));

                process_command( uart );
                WPRINT_APP_INFO(("Command Entered \r\n"));
                snprintf(command, 50, "%s", console_buffer);
                WPRINT_APP_INFO(("Current command %s\r\n", command ));
                console_buffer[0] = '\0';
                console_cursor_position = 0;
            }
            break;

        case 27: /* escape char */

            if (receiving_cert == TRUE)
            {
                console_insert_char( c );
            }
            break;

        case '\b': /* backspace */

            if (receiving_cert == TRUE)
            {
                console_insert_char( c );
            }
            break;

        case '\x7F': /* backspace */
                //console_do_backspace( );
            if (receiving_cert == TRUE)
            {
                console_insert_char( c );
            }
            break;

        case 16: /* ctrl-p */
                //console_do_up( );
            if (receiving_cert == TRUE)
            {
                console_insert_char( c );
            }
            break;

        case 14: /* ctrl-n */
                //console_do_down( );
            if (receiving_cert == TRUE)
            {
                console_insert_char( c );
            }
            break;

        case 2: /* ctrl-b */
                //console_do_left( );
            if (receiving_cert == TRUE)
            {
                console_insert_char( c );
            }
            break;

        case 6: /* ctrl-f */
                //console_do_right( );
            if (receiving_cert == TRUE)
            {
                console_insert_char( c );
            }
            break;

        case 1: /* ctrl-a */
                //console_do_home( );
            if (receiving_cert == TRUE)
            {
                console_insert_char( c );
            }
            break;

        case 5: /* ctrl-e */
                //console_do_end( );
            if (receiving_cert == TRUE)
            {
                console_insert_char( c );
            }
            break;

        case 4: /* ctrl-d */
                //console_do_delete( );
            if (receiving_cert == TRUE)
            {
                console_insert_char( c );
            }
            break;

        default:

            if ( ( c > 31 ) && ( c < 127 ) )
            { /* limit to printables */
                //WPRINT_APP_INFO(("Process printable character \r\n"));
                if ( strlen( console_buffer ) + 1 < 4096 )
                {
                    //cons.console_current_line = 0;
                    console_insert_char( c );
                    if ( strlen( console_buffer ) == 6 )
                    {
                          //check if we are receiving certificate
                          check_for_set_cert_command(uart);
                    }
                }
                else
                {
                    //interface = uart;
                    process_command( uart );

                    //maybe add a command queue for processing multiple recieved commands
                    WPRINT_APP_INFO(("Command full \r\n"));
                    snprintf(command, 10, "%s", console_buffer);
                    WPRINT_APP_INFO(("Current command %s\r\n", command ));
                    console_buffer[0] = '\0';
                    console_cursor_position = 0;
                }
            }
            else
            {
                if (receiving_cert == TRUE)
                {
                    console_insert_char( c );
                }
                    //send_charstr( console_bell_string );
            }
            break;
    }
    //cons.in_process_char = WICED_FALSE;
    //return err;
}

//Certificate includes new line characters, so we need to ignore new line and line feed characters
//and treat them as characters
void check_for_set_cert_command( interface_t interface )
{

    //should probably use a mutex or semaphore to lock command processing from addtional commands before processing addtional
    char command_value_string[2];
    int  command_value_int;
    char *command_ptr = console_buffer;
    interface_command_recieved_on = interface;
    //interface = interface;

    memcpy( command_value_string, console_buffer, 2);
    /* Get command length */
    parameter_length = new_hex_str_to_int(command_value_string);

    WPRINT_APP_INFO(("Param Length %d\r\n", parameter_length ));

    command_ptr++;
    command_ptr++;

    /* Get integer value of string byte */
    memcpy( command_value_string, command_ptr, 2);
    command_value_int = new_hex_str_to_int(command_value_string);

    command_ptr++;
    command_ptr++;

    switch (command_value_int)
    {
    case SET_TO_DEFAULT:

        WPRINT_APP_INFO(("Set to defualt command\r\n"));
        current_action = set_default;
        break;

    case SET_VALUE:

        WPRINT_APP_INFO(("Set value command \r\n"));
        current_action = set_value;

        //Process action for property
        memcpy(command_value_string, command_ptr, 2);
        command_value_int = new_hex_str_to_int(command_value_string);

        if (MQTT_PEM_CERTIFICATE == command_value_int){

            WPRINT_APP_INFO(("Prepare to recieve certificate, ignore new line and line feed \r\n"));
            receiving_cert = TRUE;
        }

        break;

    case GET_VALUE:

        WPRINT_APP_INFO(("Get value \r\n"));
        current_action = get_value;

        break;

    default:
        break;
    }
    //Process action for property
    memcpy(command_value_string, command_ptr, 2);
    command_value_int = new_hex_str_to_int(command_value_string);

    command_ptr++;
    command_ptr++;

    //process_action( command_value, command_ptr );
}
/*
 * Process base command type
 * Parameters
 * interface: the interface the command was received on. Need to know as the permission are different
 */
void process_command( interface_t interface )
{

    //should probably use a mutex or semaphore to lock command processing from addtional commands before processing addtional
    char command_value_string[2];
    int command_value_int;
    char *command_ptr = console_buffer;
    interface_command_recieved_on = interface;
    //interface = interface;

    memcpy( command_value_string, console_buffer, 2);
    /* Get command length */
    parameter_length = new_hex_str_to_int(command_value_string);

    WPRINT_APP_INFO(("Command Length %d\r\n", parameter_length ));

    command_ptr++;
    command_ptr++;

    /* Get integer value of string byte */
    memcpy( command_value_string, command_ptr, 2);
    command_value_int = new_hex_str_to_int(command_value_string);

    command_ptr++;
    command_ptr++;

    switch (command_value_int)
    {
    case SET_TO_DEFAULT:

        WPRINT_APP_INFO(("Set to defualt command\r\n"));
        current_action = set_default;
        break;

    case SET_VALUE:

        WPRINT_APP_INFO(("Set value command \r\n"));
        current_action = set_value;
        break;

    case GET_VALUE:

        WPRINT_APP_INFO(("Get value \r\n"));
        current_action = get_value;
        break;

    default:
        break;
    }

    /*  Process action for property  */
    memcpy(command_value_string, command_ptr, 2);
    command_value_int = new_hex_str_to_int(command_value_string);

    command_ptr++;
    command_ptr++;

    process_action( command_value_int, command_ptr );
}
/*
 * Match specific property the action should be done for
 */
void process_action(int property, char *parameters)
{
    /*  DCT parameters to be modified */
    platform_dct_wifi_config_t  *wifi_config_dct = NULL;
    skyplug_config_dct_t        *app_dct         = NULL;

    /*  ok to lock two sections at a time?? if not, do in case */

    /* get the App config section for modifying, any memory allocation required would be done inside wiced_dct_read_lock() */
    wiced_dct_read_lock( (void**) &app_dct, WICED_TRUE, DCT_APP_SECTION, 0, sizeof( *app_dct ) );

    /* get the wi-fi config section for modifying, any memory allocation required would be done inside wiced_dct_read_lock() */
    wiced_dct_read_lock( (void**) &wifi_config_dct, WICED_TRUE, DCT_WIFI_CONFIG_SECTION, 0, sizeof( *wifi_config_dct ) );


    switch (property)
    {
    /*===========================================================================*/
    case DEVICE_ID:
        WPRINT_APP_INFO(("Device id \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value:/* Test set value command: 040101test  */

            /* Clear data */
            memset(app_dct->skyplug_device_id, 0, 20);

            /*  Save new value to dct struct  */
            memcpy(app_dct->skyplug_device_id, parameters, parameter_length);

            /*  */
            WPRINT_APP_INFO( ( "Device id SET TO == %s\r\n", app_dct->skyplug_device_id ) );

            /*  Save in dct  */
            wiced_dct_write( (const void*) &(app_dct->skyplug_device_id), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_device_id), sizeof(app_dct->skyplug_device_id) );

            break;

        case get_value://0a0201

            /* Print Device ID */
            WPRINT_APP_INFO( ( "Device id == %s\r\n", app_dct->skyplug_device_id ) );

            break;

        default:

            break;
        }

        break;
    /*===========================================================================*/
    case FIRMWARE_VERSION:

        WPRINT_APP_INFO(("Firmware version \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value:/* Test set value command: 040102test  */

            /* Clear data */
            memset(app_dct->skyplug_firmware_version, 0, 20);

            /*  Save new value to dct struct  */
            memcpy(app_dct->skyplug_firmware_version, parameters, parameter_length);

            /*  */
            WPRINT_APP_INFO( ( "Firmware Version SET TO == %s\r\n", app_dct->skyplug_firmware_version ) );

            /*  Save in dct  */
            wiced_dct_write( (const void*) &(app_dct->skyplug_firmware_version), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_firmware_version), sizeof(app_dct->skyplug_firmware_version) );

            break;

        case get_value:

            WPRINT_APP_INFO( ( "%s\r\n", app_dct->skyplug_firmware_version ) );
            break;

        default:

            break;
        }

        break;
    /*===========================================================================*/
    case API_VERSION:
        WPRINT_APP_INFO(("Api Version \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value:/* Test set value command: 040103test  */

            /* Clear data */
            memset(app_dct->skyplug_api_version, 0, 20);

            /*  Save new value to dct struct  */
            memcpy(app_dct->skyplug_api_version, parameters, parameter_length);

            /*  */
            WPRINT_APP_INFO( ( "API Version SET TO == %s\r\n", app_dct->skyplug_api_version ) );

            /*  Save in dct  */
            wiced_dct_write( (const void*) &(app_dct->skyplug_api_version), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_api_version), sizeof(app_dct->skyplug_api_version) );

            break;

        case get_value:

            WPRINT_APP_INFO( ( "%s\r\n", app_dct->skyplug_api_version ) );
            break;

        default:

            break;
        }

        break;
    /*===========================================================================*/
    case DEVICE_KEY:
        WPRINT_APP_INFO(("Device Key \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value:/* Test set value command: 040104test  */

            /* Clear data */
            memset(app_dct->skyplug_device_key, 0, 20);

            /*  Save new value to dct struct  */
            memcpy(app_dct->skyplug_device_key, parameters, parameter_length);

            /*  */
            WPRINT_APP_INFO( ( "Device Key SET TO == %s\r\n", app_dct->skyplug_device_key ) );

            /*  Save in dct  */
            wiced_dct_write( (const void*) &(app_dct->skyplug_device_key), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_device_key), sizeof(app_dct->skyplug_device_key) );

            break;

        case get_value:

            WPRINT_APP_INFO( ( "%s\r\n", app_dct->skyplug_device_key ) );
            break;

        default:

            break;
        }

        break;
    /*===========================================================================*/
    case MQTT_ENDPOINT:
        WPRINT_APP_INFO(("MQTT Endpoint \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value:/* Test set value command: 040105test  */

            /* Clear data */
            memset(app_dct->skyplug_mqtt_endpoint, 0, 256);

            /*  Save new value to dct struct  */
            memcpy(app_dct->skyplug_mqtt_endpoint, parameters, parameter_length);

            /*  */
            WPRINT_APP_INFO( ( "Device id SET TO == %s\r\n", app_dct->skyplug_mqtt_endpoint ) );

            /*  Save in dct  */
            wiced_dct_write( (const void*) &(app_dct->skyplug_mqtt_endpoint), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_mqtt_endpoint), sizeof(app_dct->skyplug_mqtt_endpoint) );

            break;

        case get_value:

            WPRINT_APP_INFO( ( "%s\r\n", app_dct->skyplug_mqtt_endpoint ) );
            break;

        default:

            break;
        }

        break;
    /*===========================================================================*/
    case MQTT_PEM_CERTIFICATE:
        WPRINT_APP_INFO(("Pem Cert \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value://040106test

            //Write New Value
            //clear first with memset
            //need to verify certificate is correct length first
            memcpy(&security_dct.certificate, parameters, parameter_length);
            WPRINT_APP_INFO( ( "Recieved Cert = %s\r\n", security_dct.certificate ) );
            /* Write the uploaded certificate and private key into security section of DCT */
            if ( security_dct.certificate[0] != '\0')// && security_dct.private_key[0] != '\0' )
            {
                wiced_dct_write( &security_dct, DCT_SECURITY_SECTION, 0, sizeof( security_dct ) );
                security_dct.certificate[0] = '\0';
                //security_dct.private_key[0] = '\0';
            }

            break;

        case get_value:

            //PROBABLY SHOULD NOT ALLOW THIS TO BE READ....
            break;

        default:

            break;
        }

        break;
    /*===========================================================================*/
    case MQTT_PUBLIC_KEY://??Is this needed

        WPRINT_APP_INFO(("Public Key \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value:

            break;

        case get_value:

            break;

        default:

            break;
        }

        break;
    /*===========================================================================*/
    case MQTT_PRIVATE_KEY:

        WPRINT_APP_INFO(("Private Key \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value:

            //Write New Value
            //memcpy(app_dct->skyplug_mqtt_private_key, parameters, parameter_length);

            memcpy(&security_dct.private_key/*need to set size*/, parameters, parameter_length);
            /* Write the uploaded certificate and private key into security section of DCT */
            if ( security_dct.private_key[0] != '\0')// && security_dct.private_key[0] != '\0' )
            {
                wiced_dct_write( &security_dct, DCT_SECURITY_SECTION, 0, sizeof( security_dct ) );
                security_dct.private_key[0] = '\0';
                //security_dct.private_key[0] = '\0';
            }

            break;

        case get_value:

            //PROBABLY SHOULD NOT ALLOW THIS TO BE READ....
            break;

        default:

            break;
        }

        break;
    /*===========================================================================*/
    case WIFI_SSID:

        WPRINT_APP_INFO(("WiFi SSID \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value:

            /* get the wi-fi config section for modifying, any memory allocation required would be done inside wiced_dct_read_lock() */
            //wiced_dct_read_lock( (void**) &wifi_config_dct, WICED_TRUE, DCT_WIFI_CONFIG_SECTION, 0, sizeof( *wifi_config_dct ) );
            wifi_config_dct->stored_ap_list[0].details.SSID.length = parameter_length;
            memset( wifi_config_dct->stored_ap_list[0].details.SSID.value, 0, sizeof(wifi_config_dct->stored_ap_list[0].details.SSID.value) );
            memcpy( (char*)wifi_config_dct->stored_ap_list[0].details.SSID.value, parameters, parameter_length);

            /* Write config */
            wiced_dct_write( (const void*) wifi_config_dct, DCT_WIFI_CONFIG_SECTION, 0, sizeof(platform_dct_wifi_config_t) );

            //wiced_dct_read_unlock( (void*) wifi_config_dct, WICED_TRUE );

            break;

        case get_value:

            //WPRINT_APP_INFO( ( "%s\r\n", app_dct->skyplug_wifi_ssid ) );
            break;

        default:

            break;
        }

        break;
    /*===========================================================================*/
    case WIFI_AUTH_TYPE:

        WPRINT_APP_INFO(("WiFi Auth Type \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value:

            //Write New Value
            //read this from wifi_config_dct not app_dct. Better to use existing??
            break;

        case get_value:

            //WPRINT_APP_INFO( ( "%s\r\n", app_dct->skyplug_wifi_auth_type ) );
            break;

        default:
            break;
        }

        break;
    /*===========================================================================*/
    case WIFI_USERNAME:

        WPRINT_APP_INFO(("WiFi Username \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value:

            //Write New Value
            //memcpy(app_dct->skyplug_wifi_username, parameters, parameter_length);
            break;

        case get_value:

            //WPRINT_APP_INFO( ( "%s\r\n", app_dct->skyplug_wifi_username ) );
            break;

        default:
            break;
        }

        break;
    /*===========================================================================*/
    case WIFI_PASSWORD:

        WPRINT_APP_INFO(("Wifi Password \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value:

            /* get the wi-fi config section for modifying, any memory allocation required would be done inside wiced_dct_read_lock() */
            //wiced_dct_read_lock( (void**) &wifi_config_dct, WICED_TRUE, DCT_WIFI_CONFIG_SECTION, 0, sizeof( *wifi_config_dct ) );

            /* Save credentials for non-enterprise AP */
            memcpy((char*)wifi_config_dct->stored_ap_list[0].security_key, parameters, parameter_length);
            wifi_config_dct->stored_ap_list[0].security_key_length = parameter_length;

            /* Write config */
            wiced_dct_write( (const void*) wifi_config_dct, DCT_WIFI_CONFIG_SECTION, 0, sizeof(platform_dct_wifi_config_t) );

            //wiced_dct_read_unlock( (void*) wifi_config_dct, WICED_TRUE );

            break;

        case get_value:

            //WPRINT_APP_INFO( ( "%s\r\n", app_dct->skyplug_wifi_password ) );
            break;

        default:

            break;
        }

        break;
    /*===========================================================================*/
    case SCANNED_SSIDS:

        WPRINT_APP_INFO(("Scanned SSIDs \r\n"));

        switch (current_action)
        {
        case set_default:
            //RETURN ERROR INVALID
            break;

        case set_value:

            //Write New Value
            break;

        case get_value:

            break;

        default:

            break;
        }

        break;
    /*===========================================================================*/
    case HAS_FAN:

        WPRINT_APP_INFO(("Has Fan \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value:

            /* Clear data */
            memset(app_dct->skyplug_has_fan, 0, 20);

            /*  Save new value to dct struct  */
            memcpy(app_dct->skyplug_has_fan, parameters, parameter_length);

            /*  */
            WPRINT_APP_INFO( ( "Device id SET TO == %s\r\n", app_dct->skyplug_has_fan ) );

            /*  Save in dct  */
            wiced_dct_write( (const void*) &(app_dct->skyplug_has_fan), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_has_fan), sizeof(app_dct->skyplug_has_fan) );

            break;

        case get_value:

            WPRINT_APP_INFO( ( "%s\r\n", app_dct->skyplug_has_fan ) );
            break;

        default:

            break;
        }

        break;
    /*===========================================================================*/
    case FAN_SPEEDS:

        WPRINT_APP_INFO(("Fan speeds \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value:

            /* Clear data */
            memset(app_dct->skyplug_fan_speeds, 0, 20);

            /*  Save new value to dct struct  */
            memcpy(app_dct->skyplug_fan_speeds, parameters, parameter_length);

            /*  */
            WPRINT_APP_INFO( ( "Device id SET TO == %s\r\n", app_dct->skyplug_fan_speeds ) );

            /*  Save in dct  */
            wiced_dct_write( (const void*) &(app_dct->skyplug_fan_speeds), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_fan_speeds), sizeof(app_dct->skyplug_fan_speeds) );

            break;

        case get_value:

            WPRINT_APP_INFO( ( "%s\r\n", app_dct->skyplug_fan_speeds ) );
            break;

        default:

            break;
        }

        break;
    /*===========================================================================*/
    case IS_FAN_REVERSIBLE:

        WPRINT_APP_INFO(("Is fan reversible \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value:

            /* Clear data */
            memset(app_dct->skyplug_is_fan_reversible, 0, 20);

            /*  Save new value to dct struct  */
            memcpy(app_dct->skyplug_is_fan_reversible, parameters, parameter_length);

            /*  */
            WPRINT_APP_INFO( ( "Device id SET TO == %s\r\n", app_dct->skyplug_is_fan_reversible ) );

            /*  Save in dct  */
            wiced_dct_write( (const void*) &(app_dct->skyplug_is_fan_reversible), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_is_fan_reversible), sizeof(app_dct->skyplug_is_fan_reversible) );

            break;

        case get_value:

            WPRINT_APP_INFO( ( "%s\r\n", app_dct->skyplug_is_fan_reversible ) );
            break;

        default:

            break;
        }

        break;
    /*===========================================================================*/
    case LIGHT_MIN_DIM_LEVEL:

        WPRINT_APP_INFO(("Light min dim level \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value:

            /* Clear data */
            memset(app_dct->skyplug_light_min_dim_level, 0, 20);

            /*  Save new value to dct struct  */
            memcpy(app_dct->skyplug_light_min_dim_level, parameters, parameter_length);

            /*  */
            WPRINT_APP_INFO( ( "Device id SET TO == %s\r\n", app_dct->skyplug_light_min_dim_level ) );

            /*  Save in dct  */
            wiced_dct_write( (const void*) &(app_dct->skyplug_light_min_dim_level), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_light_min_dim_level), sizeof(app_dct->skyplug_light_min_dim_level) );

            break;

        case get_value:

            WPRINT_APP_INFO( ( "%s\r\n", app_dct->skyplug_light_min_dim_level ) );
            break;

        default:

            break;
        }

        break;
    /*===========================================================================*/
    case LIGHT_TRANSITION_TIME:

        WPRINT_APP_INFO(("Light transition time \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value:

            /* Clear data */
            memset(app_dct->skyplug_light_transition_time, 0, 20);

            /*  Save new value to dct struct  */
            memcpy(app_dct->skyplug_light_transition_time, parameters, parameter_length);

            /*  */
            WPRINT_APP_INFO( ( "Device id SET TO == %s\r\n", app_dct->skyplug_light_transition_time ) );

            /*  Save in dct  */
            wiced_dct_write( (const void*) &(app_dct->skyplug_light_transition_time), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_light_transition_time), sizeof(app_dct->skyplug_light_transition_time) );

            break;

        case get_value:

            WPRINT_APP_INFO( ( "%s\r\n", app_dct->skyplug_light_transition_time ) );
            break;

        default:

            break;
        }

        break;
    /*===========================================================================*/
    case IS_LIGHT_DIMMABLE:

        WPRINT_APP_INFO(("Is light dimmable \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value:

            /* Clear data */
            memset(app_dct->skyplug_is_light_dimmable, 0, 20);

            /*  Save new value to dct struct  */
            memcpy(app_dct->skyplug_is_light_dimmable, parameters, parameter_length);

            /*  */
            WPRINT_APP_INFO( ( "Device id SET TO == %s\r\n", app_dct->skyplug_is_light_dimmable ) );

            /*  Save in dct  */
            wiced_dct_write( (const void*) &(app_dct->skyplug_is_light_dimmable), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_is_light_dimmable), sizeof(app_dct->skyplug_is_light_dimmable) );

            break;

        case get_value:

            WPRINT_APP_INFO( ( "%s\r\n", app_dct->skyplug_is_light_dimmable ) );
            break;

        default:

            break;
        }

        break;
    /*===========================================================================*/
    case WIFI_STATUS:

        WPRINT_APP_INFO(("WiFi Status \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value:

            break;

        case get_value:

            break;

        default:

            break;
        }

        break;
    /*===========================================================================*/
    case LIGHT_POWER://01028100 01018100
        //WPRINT_APP_INFO(("Light power \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value:

            //Write New Value
            update_aws_shadow();
            break;

        case get_value:

            WPRINT_APP_INFO( ( "Stored Light Power %d\r\n", skyplug_light_power) );
            break;

        default:

            break;
        }

        break;
    /*===========================================================================*/
    case LIGHT_LEVEL:

        WPRINT_APP_INFO(("Light Level \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value:

            //Write New Value
            update_aws_shadow();
            break;

        case get_value:

            WPRINT_APP_INFO( ( "Light Level %d\r\n", skyplug_light_level ) );
            break;

        default:
            break;
        }

        break;
    /*===========================================================================*/
    case FAN_POWER:

        WPRINT_APP_INFO(("Fan power \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value:

            //Write New Value
            update_aws_shadow();
            break;

        case get_value:

            WPRINT_APP_INFO( ( "Fan Power %d\r\n", skyplug_fan_power ) );
            break;

        default:

            break;
        }

        break;

    /*===========================================================================*/
    case FAN_SPEED:

        WPRINT_APP_INFO(("Fan speed \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value:

            //Write New Value
            update_aws_shadow();
            break;

        case get_value://01028400

            WPRINT_APP_INFO( ( "Fan Level %d\r\n", skyplug_fan_level ) );
            break;

        default:
            break;
        }

        break;

    /*===========================================================================*/
    case FAN_DIRECTION:
        WPRINT_APP_INFO(("Fan direction \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value:

            //Check parameter length
            //Make sure value is within range
            //Set value
            //Write New Value
            update_aws_shadow();
            break;

        case get_value:

            WPRINT_APP_INFO( ( "Fan Direction %d\r\n", skyplug_fan_direction ) );
            break;

        default:

            break;
        }

        break;

    default:

        WPRINT_APP_INFO(("Invalid Property \r\n"));
        //unknown property or status value
        break;
    }

    /* release the read lock */
    wiced_dct_read_unlock( (void*) wifi_config_dct, WICED_TRUE );
    wiced_dct_read_unlock( app_dct, WICED_TRUE );
}


/*
 ******************************************************************************
 * Insert a character into the current line at the cursor position and update the screen.
 *
 * @param[in] c  The character to be inserted.
 *
 * @return    void
 */
void console_insert_char( char c )
{
    uint32_t i;
    uint32_t len = strlen( console_buffer );

    /* move the end of the line out to make space */
    for ( i = len + 1; i > console_cursor_position; i-- )
    {
        console_buffer[i] = console_buffer[i - 1];
    }

    /* insert the character */
    len++;
    console_buffer[console_cursor_position] = c;

    WPRINT_APP_INFO(("Current command %s\r\n", console_buffer));
    /* print out the modified part of the ConsoleBuffer */
    //send_str( &cons.console_buffer[cons.console_cursor_position] );

    /* move the cursor back to where it's supposed to be */
    console_cursor_position++;
    for ( i = len; i > console_cursor_position; i-- )
    {
        //send_char( '\b' );
    }
}


/*!
 ******************************************************************************
 * Convert a hexidecimal string to an integer.
 *
 * @param[in] hex_str  The string containing the hex value.
 *
 * @return    The value represented by the string.
 */

int new_hex_str_to_int( const char* hex_str )
{
    int n = 0;
    uint32_t value = 0;
    int shift = 7;
    while ( hex_str[n] != '\0' && n < 8 )
    {
        if ( hex_str[n] > 0x21 && hex_str[n] < 0x40 )
        {
            value |= ( hex_str[n] & 0x0f ) << ( shift << 2 );
        }
        else if ( ( hex_str[n] >= 'a' && hex_str[n] <= 'f' ) || ( hex_str[n] >= 'A' && hex_str[n] <= 'F' ) )
        {
            value |= ( ( hex_str[n] & 0x0f ) + 9 ) << ( shift << 2 );
        }
        else
        {
            break;
        }
        n++;
        shift--;
    }

    return ( value >> ( ( shift + 1 ) << 2 ) );
}
