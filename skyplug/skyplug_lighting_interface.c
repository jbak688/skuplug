#include <stdlib.h>
#include <string.h>
#include "wiced.h"

#include <wiced_utilities.h>
#include <resources.h>
#include "wiced_framework.h"
#include "skyplug_lighting_interface.h"


/*
 *  Set the light level to value in range {0x00, 0xFF}
 */
wiced_result_t skyplug_set_light_level(uint8_t light_level){
    return WICED_SUCCESS;
}

/*
 *  Set the light power to either on 0x01 or off 0x00
 */
wiced_result_t skyplug_set_light_power(uint8_t light_power){
    return WICED_SUCCESS;
}

/*
 *  Set the light min dim level to value in range {0x00, 0x06}
 */
wiced_result_t skyplug_set_light_min_dim_level(uint8_t min_dim_level){
    return WICED_SUCCESS;
}
