

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "wiced.h"
#include "wiced_utilities.h"
#include "skyplug.h"
#include "wiced_log.h"
#include "wiced_framework.h"
#include "wiced_crypto.h"
#include "mbedtls/aes.h"
#include "skyplug_config_dct.h"
#include "skyplug_security_interface.h"

#define AES_CBC_IV_LENGTH        16              /* Length of the AES-CBC initialization vector in octets */
#define AES_CBC_KEY_LENGTH       128             /* Length of the AES-CBC key in bits */

//#define DEBUG_SECURITY

uint8_t initialization_vector[16];

aes_context_t context_aes;

void run_encryption(uint8_t *unencrypted_packet, uint8_t packet_length, uint8_t *encrypted_packet)
{
    int index;
    uint8_t *start_data = unencrypted_packet;
    uint8_t *head = unencrypted_packet;
    uint8_t padded_data[16];
    uint8_t encypted_data[16];
    uint8_t unencypted_data[16];

    #ifdef DEBUG_SECURITY
    WPRINT_APP_INFO(("\r\nBase Packet\r\n"));

    for (index = 0; index < packet_length; index++)
    {
        WPRINT_APP_INFO(("%02x ", *start_data));
        start_data++;
    }
    #endif 

    start_data = head;
    add_padding(start_data, packet_length, padded_data);

    #ifdef DEBUG_SECURITY
    WPRINT_APP_INFO(("\r\nPadded Packet\r\n"));

    for (index = 0; index < 16; index++)
    {
        WPRINT_APP_INFO(("%02x", padded_data[index]));
    }
    #endif

    encrypt_packet(padded_data, 16, encypted_data);

    #ifdef DEBUG_SECURITY
    WPRINT_APP_INFO(("\r\nEncrypted Packet\r\n"));

    for (index = 0; index < 16; index++)
    {
        WPRINT_APP_INFO(("%02x", encypted_data[index]));
    }
    #endif

    memcpy(encrypted_packet, encypted_data, 16);
    decrypt_packet(encypted_data, 16, unencypted_data);

    #ifdef DEBUG_SECURITY
    WPRINT_APP_INFO(("\r\nDecrypted Packet\r\n"));

    for (index = 0; index < 16; index++)
    {
        WPRINT_APP_INFO(("%02x", unencypted_data[index]));
    }
    #endif

}


void add_padding(uint8_t *unpadded_data, uint8_t unpadded_data_length, uint8_t *padded_data){

	int index;

    uint8_t *head = unpadded_data;
    uint8_t padded[16];

    memset(padded, 0, 16);
    //WPRINT_APP_INFO(("Copied Data \r\n"));

    /* copy unpadded data*/
    for (index = 0; index < unpadded_data_length; index++)
    {
        padded[index] = *head;
        //WPRINT_APP_INFO(("%02x", padded[index]));
        head++;
    }

    uint8_t pad = (uint8_t)(16 - unpadded_data_length);

    /*  add pad values */
    for (index = unpadded_data_length; index < 16; index++)
    {
        padded[index] =  pad;
    }

    memcpy(padded_data, padded, 16);
}

void encrypt_packet(uint8_t *unencrypted_packet, uint8_t packet_length, uint8_t *encrypted_packet)
{


    uint8_t cipher_text[16];
    uint8_t iv[16];

    /* app_dct config */
    skyplug_config_dct_t *app_dct= NULL;

    /* get app dct  */
    wiced_dct_read_lock( (void**) &app_dct, WICED_TRUE, DCT_APP_SECTION, 0, sizeof( *app_dct ) );

    /* get key from dct */
    memcpy(iv, initialization_vector, AES_CBC_IV_LENGTH);

    /* clear memory */
    memset(&context_aes, 0, sizeof(context_aes));

    /* set encryption key */
    mbedtls_aes_setkey_enc(&context_aes, app_dct->skyplug_device_key, AES_CBC_KEY_LENGTH);

    /* encrypt data */
    mbedtls_aes_crypt_cbc(&context_aes, AES_ENCRYPT, packet_length, iv, (unsigned char*)unencrypted_packet, cipher_text);

    /*  copy encrypted text */
    memcpy(encrypted_packet, cipher_text, 16);

    /* unlock dct */
    wiced_dct_read_unlock( (void*) app_dct, WICED_TRUE );

}

void decrypt_packet(uint8_t *encrypted_packet, uint8_t packet_length, uint8_t *decrypted_packet)
{

    WPRINT_APP_INFO(("\r\nEncrypted Packet To decrypt\r\n"));

    for (int index = 0; index < 16; index++)
    {
        WPRINT_APP_INFO(("%02x", encrypted_packet[index]));
    }
    
    uint8_t plain_text[16];
    uint8_t iv[16];

    /* app_dct config */
    skyplug_config_dct_t *app_dct= NULL;

    /* get app dct  */
    wiced_dct_read_lock( (void**) &app_dct, WICED_TRUE, DCT_APP_SECTION, 0, sizeof( *app_dct ) );

    /* get key from dct*/
    memcpy(iv, initialization_vector, AES_CBC_IV_LENGTH);

    /* clear memory */
    memset(&context_aes, 0, sizeof(context_aes));

    /* set key*/
    mbedtls_aes_setkey_dec(&context_aes, app_dct->skyplug_device_key, AES_CBC_KEY_LENGTH);

    /* decrypt data */
    mbedtls_aes_crypt_cbc(&context_aes, AES_DECRYPT, packet_length, iv, (unsigned char*)encrypted_packet, plain_text);

    /* copy decrypted packet*/
    memcpy(decrypted_packet, plain_text, 16);

    /* unlock dct */
    wiced_dct_read_unlock( (void*) app_dct, WICED_TRUE );
}

uint8_t* generate_initialization_vector()
{
    int idx;

	if (WICED_SUCCESS == wiced_crypto_get_random( initialization_vector, 16 ))
	{
        WPRINT_APP_INFO(("Generated initialization Vector\r\n"));

        for(idx = 0; idx < 16; idx++)
        {
            WPRINT_APP_INFO(("%02x ", initialization_vector[idx]));
        }

		return initialization_vector;
	} 
    else
	{
	    return 0;
	}
}













