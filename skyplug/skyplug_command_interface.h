
#include "wiced.h"
#include "resources.h"

typedef enum{
    SKYPLUG_SUCCESS = 0,
    SKYPLUG_FAILURE,
    SKYPLUG_INVALID_COMMAND,
    SKYPLUG_INVALID_PROPERTY_OR_STATUS_ID,
    SKYPLUG_MAX_PARAMETER_LENGTH_EXCEEDED,
    SKYPLUG_INVALID_ACTION,
    SKYPLUG_INVALID_CHECKSUM,
    SKYPLUG_ACCESS_DENIED,
    SKYPLUG_NOT_SETTABLE,
    SKYPLUG_NOT_GETTABLE,
    SKYPLUG_INVALID_START_SEQUENCE
}skyplug_command_error_t;

typedef enum
{
    set_default = 0,
    set_value = 1,
    get_value = 2,
    not_set = 4
}actions_t;

typedef enum
{
    ble = 0,
    uart = 1,
    na = 2
}interface_t;

void console_process_char( char c );

void process_command( interface_t interface );

void console_insert_char( char c );

int new_hex_str_to_int( const char* hex_str );

void process_action(int property, char *parameters);

void check_for_set_cert_command( interface_t interface );

void process_bt_response(uint8_t *unprocessed_response_data, uint8_t unprocessed_response_data_length);

extern wiced_bool_t receiving_certificate;
extern int cert_length;