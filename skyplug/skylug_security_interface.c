

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "wiced.h"
#include "wiced_utilities.h"
#include "skyplug.h"
#include "wiced_log.h"
#include "wiced_framework.h"
#include "wiced_crypto.h"
#include "mbedtls/aes.h"

uint8_t* initialization_vector;


uint8_t* add_padding(uint8_t *unpadded_data, uint8_t updadded_data_length){

	int index;

    uint8_t *padded_data = (uint8_t*)malloc(16);//needs to be 16 bytes 
    uint8_t *padded_data_head = padded_data;

    memcpy(padded_data, unpadded_data, unpadded_data_length);

    uint8_t pad = (uint8_t)(16 - unpadded_data_length);

    for (index = unpadded_data_length; index < 16; index++){
        *padded_data = pad;
        padded_data++;
    }
    WPRINT_APP_INFO( ( "PADDED DATA") );
    for (index = 0; index < 16; index++){
        WPRINT_APP_INFO( ( "%02x", *padded_data_head) );
        padded_data_head++;
    }

}

uint8_t* encrypt_packet(uint8_t *unencrypted_packet, uint8_t packet_length)
{
	uint8_t *encrypted_packet;




	return encrypted_packet;
}

uint8_t* decrypt_packet(uint8_t *encrypted_packet, uint8_t packet_length)
{

	uint8_t *unencrypted_packet;





	return unencrypted_packet;

}

uint8_t* generate_initialization_vector()
{

	if (WICED_SUCCESS == wiced_result_t wiced_crypto_get_random( initialization_vector, 16 ))
	{
		return initialization_vector;
	}
}