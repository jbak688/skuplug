
#include "wiced.h"
#include "resources.h"




void run_encryption(uint8_t *unencrypted_packet, uint8_t packet_length, uint8_t *encrypt_packet);

void add_padding(uint8_t *unpadded_data, uint8_t unpadded_data_length, uint8_t *padded_data);

void encrypt_packet(uint8_t *unencrypted_packet, uint8_t packet_length, uint8_t *encrypted_packet);

void decrypt_packet(uint8_t *encrypted_packet, uint8_t packet_length, uint8_t *decrypted_packet);

uint8_t* generate_initialization_vector();