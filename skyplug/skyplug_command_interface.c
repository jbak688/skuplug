/*
 * Copyright 2017, Cypress Semiconductor Corporation or a subsidiary of
 * Cypress Semiconductor Corporation. All Rights Reserved.
 *
 * This software, associated documentation and materials ("Software"),
 * is owned by Cypress Semiconductor Corporation
 * or one of its subsidiaries ("Cypress") and is protected by and subject to
 * worldwide patent protection (United States and foreign),
 * United States copyright laws and international treaty provisions.
 * Therefore, you may use this Software only as provided in the license
 * agreement accompanying the software package from which you
 * obtained this Software ("EULA").
 * If no EULA applies, Cypress hereby grants you a personal, non-exclusive,
 * non-transferable license to copy, modify, and compile the Software
 * source code solely for use in connection with Cypress's
 * integrated circuit products. Any reproduction, modification, translation,
 * compilation, or representation of this Software except as specified
 * above is prohibited without the express written permission of Cypress.
 *
 * Disclaimer: THIS SOFTWARE IS PROVIDED AS-IS, WITH NO WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, NONINFRINGEMENT, IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. Cypress
 * reserves the right to make changes to the Software without notice. Cypress
 * does not assume any liability arising out of the application or use of the
 * Software or any product or circuit described in the Software. Cypress does
 * not authorize its products for use in any products where a malfunction or
 * failure of the Cypress product may reasonably be expected to result in
 * significant property damage, injury or death ("High Risk Product"). By
 * including Cypress's product in a High Risk Product, the manufacturer
 * of such system or application assumes all risk of such use and in doing
 * so agrees to indemnify Cypress against all liability.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "wiced.h"
#include "wiced_utilities.h"
#include "skyplug.h"
#include "skyplug_command_interface.h"
#include "wiced_log.h"
#include "skyplug_config_dct.h"
#include "wiced_framework.h"
#include "skyplug_fan_interface.h"
#include "skyplug_lighting_interface.h"
#include "mbedtls/aes.h"
#include "skyplug_security_interface.h"
#include "skyplug_defaults.h"

/******************************************************
 *                    Constants
 ******************************************************/
/*  Command Interface Property ID Definitions  */
#define DEVICE_ID                   0x01
#define FIRMWARE_VERSION            0x02
#define API_VERSION                 0x03
#define DEVICE_KEY                  0x04
#define MQTT_ENDPOINT               0x05
#define MQTT_PEM_CERTIFICATE        0x06
#define MQTT_PUBLIC_KEY             0x07
#define MQTT_PRIVATE_KEY            0x08
#define WIFI_SSID                   0x09
#define WIFI_AUTH_TYPE              0x0a
#define WIFI_USERNAME               0x0b
#define WIFI_PASSWORD               0x0c
#define SCANNED_SSIDS               0x0d
#define HAS_FAN                     0x0e
#define FAN_SPEEDS                  0x0f
#define IS_FAN_REVERSIBLE           0x10
#define LIGHT_MIN_DIM_LEVEL         0x11
#define LIGHT_TRANSITION_TIME       0x12
#define IS_LIGHT_DIMMABLE           0x13




/*  Command Interface Status ID Definitions  */
#define WIFI_STATUS                 0x80
#define LIGHT_POWER                 0x81
#define LIGHT_LEVEL                 0x82
#define FAN_POWER                   0x83
#define FAN_SPEED                   0x84
#define FAN_DIRECTION               0x85

/*  Command Interface Verb Definitions  */
#define SET_TO_DEFAULT              0x00
#define SET_VALUE                   0x01
#define GET_VALUE                   0x02

#define UART_RESPONSE_START_SEQUENCE "ffffff"
#define GET_RESPONSE_BASE ""
#define SET_RESPONSE_SUCCESS        "ffffff0300ffff"
#define SET_RESPONSE_FAILE          "ffffff0301ffff"

//#define DEBUG_BT
/******************************************************************************
 *                                Static function declarations
 ******************************************************************************/

static uint16_t Fletcher16( uint8_t *data, int count );
static wiced_result_t send_response(int status, uint8_t *value, int value_length, int property);
//static void process_bt_response(uint8_t *unprocessed_response_data, uint8_t unprocessed_response_data_length);
//static uint8_t* add_padding(uint8_t *unpadded_data, uint8_t updadded_data_length);
/******************************************************************************
 *                                Structures
 ******************************************************************************/

typedef struct
{

}property_defaults_t;

/******************************************************************************
 *                                Variables Definitions
 ******************************************************************************/
/*  Buffer used for recieved UART characters */
char                                               command_response[50];
char                                               console_buffer[4096];//can we process entire cert in one command?
static uint32_t                                    console_cursor_position = 0;
static int                                         parameter_length;//using for debugging commands, remove
static uint8_t                                     message_id;//message id is the message count for commands recieved over bluetooth.  Should be reset at the beginning of each connection 

/*  Used to indicate when we are receiving a cert.  During receive, ignore new line and line feed characters*/
static wiced_bool_t                                receiving_cert;

/*  Being used for debugging print out current command buffer */
char command[2000];

/* */
actions_t current_action                    = not_set;
interface_t interface_command_recieved_on   = na;

wiced_bool_t receiving_certificate;
int cert_length;
/******************************************************************************
 *                                External Variables
 ******************************************************************************/
//extern char *skyplug_fan_level;
//extern char *skyplug_fan_power;
//extern char *skyplug_fan_direction;
//extern char *skyplug_light_level;
//extern char *skyplug_light_power;

/* store first 7 bytes to check if we are recieving certificate */
char check_for_cert[14];
extern const char *WIFI_STATUS_VALUES[];
//extern wifi_status_t wifi_status;

void console_process_char( char c )
{
    char *utility_ptr;
    switch ( c )
    {
        case 9: /* tab char */
                WPRINT_APP_INFO(("Tab \r\n"));
                if (receiving_cert == TRUE){
                    console_insert_char( c );
                }
                break;

        case 10: /* line feed */
                /* ignore it */
            WPRINT_APP_INFO(("Line Feed character \r\n"));
            if (receiving_cert == TRUE)
            {
                console_insert_char( c );
            }
            break;

        case '\r': /* newline */

            if (receiving_cert == TRUE)
            {
                console_insert_char( c );

            }
            else
            {

                process_command( uart );
                //WPRINT_APP_INFO(("Command Entered \r\n"));
                snprintf(command, sizeof(console_buffer), "%s", console_buffer);
                WPRINT_APP_INFO(("Current command %s\r\n", command ));
                console_buffer[0] = '\0';
                console_cursor_position = 0;
            }
            break;

        case 27: /* escape char */

            WPRINT_APP_INFO((" escape \r\n"));
            if (receiving_cert == TRUE)
            {
                console_insert_char( c );
            }
            break;

        case '\b': /* backspace */

            WPRINT_APP_INFO(("  backspace \r\n"));
            if (receiving_cert == TRUE)
            {
                console_insert_char( c );
            }
            break;

        case '\x7F': /* backspace */
                //console_do_backspace( );
            WPRINT_APP_INFO((" do backspace\r\n"));
            if (receiving_cert == TRUE)
            {
                console_insert_char( c );
            }
            break;

        case 16: /* ctrl-p */
                //console_do_up( );
            WPRINT_APP_INFO((" do up \r\n"));
            if (receiving_cert == TRUE)
            {
                console_insert_char( c );
            }
            break;

        case 14: /* ctrl-n */
                //console_do_down( );
            WPRINT_APP_INFO((" do down  \r\n"));
            if (receiving_cert == TRUE)
            {
                console_insert_char( c );
            }
            break;

        case 2: /* ctrl-b */
                //console_do_left( );
            WPRINT_APP_INFO((" do left \r\n"));
            if (receiving_cert == TRUE)
            {
                console_insert_char( c );
            }
            break;

        case 6: /* ctrl-f */
                //console_do_right( );
            WPRINT_APP_INFO((" do right \r\n"));
            if (receiving_cert == TRUE)
            {
                console_insert_char( c );
            }
            break;

        case 1: /* ctrl-a */
                //console_do_home( );
            WPRINT_APP_INFO(("do home \r\n"));
            if (receiving_cert == TRUE)
            {
                console_insert_char( c );
            }
            break;

        case 5: /* ctrl-e */
                //console_do_end( );
            WPRINT_APP_INFO((" do end\r\n"));
            if (receiving_cert == TRUE)
            {
                console_insert_char( c );
            }
            break;

        case 4: /* ctrl-d */
                //console_do_delete( );
            WPRINT_APP_INFO((" delete \r\n"));
            if (receiving_cert == TRUE)
            {
                console_insert_char( c );
            }
            break;

        default:
            //WPRINT_APP_INFO(("Current Buffer Length %d\r\n", strlen(  console_buffer ) ));
            if ( ( c > 31 ) && ( c < 127 ) )
            { /* limit to printables */
                //WPRINT_APP_INFO(("Process printable character \r\n"));
                if ( strlen( console_buffer ) + 1 < 4096 )
                {
                    //cons.console_current_line = 0;
                    console_insert_char( c );
                }
                else
                {
                    console_insert_char( c );
                    //interface = uart;
                    process_command( uart );

                    //maybe add a command queue for processing multiple recieved commands
                    WPRINT_APP_INFO(("Command full \r\n"));
                    snprintf(command, sizeof(console_buffer), "%s", console_buffer);
                    WPRINT_APP_INFO(("Current command %s\r\n", command ));
                    console_buffer[0] = '\0';
                    console_cursor_position = 0;
                }
            }
            else
            {
                //if (receiving_cert == TRUE)
                //{
                WPRINT_APP_INFO(("Unknown Character \r\n"));
                    console_insert_char( c );
                //}
                    //send_charstr( console_bell_string );
            }
            break;
    }
    //cons.in_process_char = WICED_FALSE;
    //return err;
}


/*
 * Process base command and command type
 *
 * Parameters
 *  interface: the interface the command was received on. Need to know as the permissionS are different
 */
void process_command( interface_t interface )
{
    /*  set to appropriate error when encountered, will be send in response packet */
    skyplug_command_error_t command_error = SKYPLUG_SUCCESS;

    //should probably use a mutex or semaphore to lock command processing from additional commands before processing additional
    char command_length[5];
    char command_value_string[2];
    int command_value_int;

    /*  use to store the checksum computed based on the recieved data. Will compare to recieved checksum  */
    char *received_checksum[4];
    uint16_t computed_checksum;

    /*  pointer for command processing, passed to subsequent function call */
    char *command_ptr = console_buffer;

    /*  save reference to buffer head */
    //char *command_data_head = console_buffer;

    /*  save the interface the command was recieved on for later reference */
    interface_command_recieved_on = interface;

    /*  If received on UART, make sure the start sequence if valid */
    if(strncmp(console_buffer, "ffffff", 6 ) == 0  && interface == uart)
    {
        WPRINT_APP_INFO(("Start Sequence Received \r\n"));

        /*  move pointer to the end of the start sequence */
        command_ptr += 6;
    }
    else if(interface == ble) /* no start sequence included with commands received over ble */
    {
        WPRINT_APP_INFO(("BLE command Received \r\n"));
    }
    else
    {
        //Invalid start sequence
        command_error = SKYPLUG_INVALID_START_SEQUENCE;
        WPRINT_APP_INFO(("Invalid Start Sequence Received \r\n"));
                            console_buffer[0] = '\0';
                    console_cursor_position = 0;
        return;
    }

    /*  first four characters = first byte of command = parameter length */
    memcpy( command_length, command_ptr, 4);

    /* should rename to command_length because that is what is actually being send*/
    parameter_length = new_hex_str_to_int(command_length);

    //recieved_checksum = (comand_data_ptr + parameter_length)
    /* copy received checksum into char buffer*/
    memcpy( received_checksum, (command_ptr + parameter_length), 4);

    /* get integer value of ascii checksum converted */
    computed_checksum = Fletcher16((uint8_t *)command_ptr, parameter_length);

    /*
     * Verify computed checksum matches the checksum received (convert checksum ascii bytes to integer value)
     */
    //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    if (0){
    if (computed_checksum == (uint16_t)(new_hex_str_to_int((const char *)received_checksum)))
    {
        WPRINT_APP_INFO(("Checksum is valid \r\n" ));
    }
    else
    {
        command_error = SKYPLUG_INVALID_CHECKSUM;
        WPRINT_APP_INFO(("Invalid checksum \r\n"));
        return;
    }}

    /* DEBUGGING */
    WPRINT_APP_INFO(("Command Length %d\r\n", parameter_length ));

    /*  Increment pointer to next byte in character array */
    command_ptr++;
    command_ptr++;
    command_ptr++;
    command_ptr++;

    /* second two characters = second byte of command = command verb */
    memcpy( command_value_string, command_ptr, 2);
    command_value_int = new_hex_str_to_int(command_value_string);

    WPRINT_APP_INFO(("Command verb: %d\r\n", command_value_int));
    /*  Increment pointer to next byte in character array */
    command_ptr++;
    command_ptr++;

    /*  verify and save verb as current action */
    switch (command_value_int)
    {
    case SET_TO_DEFAULT:

        WPRINT_APP_INFO(("Set to default command\r\n"));
        current_action = set_default;
        break;

    case SET_VALUE:

        WPRINT_APP_INFO(("Set value command \r\n"));
        current_action = set_value;
        break;

    case GET_VALUE:

        WPRINT_APP_INFO(("Get value \r\n"));
        current_action = get_value;
        break;

    default:

        WPRINT_APP_INFO(("Invalid command \r\n"));
        command_error = SKYPLUG_INVALID_ACTION;

        break;
    }

    /*  Third pair characters = third byte of command = property or status ID  */
    memcpy(command_value_string, command_ptr, 2);
    command_value_int = new_hex_str_to_int(command_value_string);

    /*  Increment pointer to next byte in character array */
    command_ptr++;
    command_ptr++;

    if (command_error == SKYPLUG_SUCCESS)
    {
        /*  base command verified, process requested action */
        process_action( command_value_int, command_ptr );
    }
    else
    {
        send_response(command_error, (uint8_t*)NULL, (int)NULL, command_value_int);
    }
}

/*
 * Process command action for specified property
 */
void process_action(int property, char *parameters)
{
    /* Used for debug printing */
    int index;

    /*  keep track of errors */
    skyplug_command_error_t command_error;// = ;

    /*  used for process ascii characters to single bytes*/
    char command_value_string[2];
    int command_value_int;
    wiced_result_t result;

    /*  DCT parameters to be modified */
    platform_dct_wifi_config_t  *wifi_config_dct = NULL;
    skyplug_config_dct_t        *app_dct         = NULL;

    /*  why did i not use a pointer here?  need to verify being used correctly */
    //static platform_dct_security_t                     security_dct;// =  malloc(sizeof(platform_dct_security_t));
    platform_dct_security_t    *security_dct = NULL;


    /* get the App config section for modifying, any memory allocation required would be done inside wiced_dct_read_lock() */
    wiced_dct_read_lock( (void**) &security_dct, WICED_TRUE, DCT_SECURITY_SECTION, 0, sizeof( *security_dct ) );

    /* Ok to lock two sections at a time?? if not, do in case */
    /* get the App config section for modifying, any memory allocation required would be done inside wiced_dct_read_lock() */
    wiced_dct_read_lock( (void**) &app_dct, WICED_TRUE, DCT_APP_SECTION, 0, sizeof( *app_dct ) );

    /* get the wi-fi config section for modifying, any memory allocation required would be done inside wiced_dct_read_lock() */
    wiced_dct_read_lock( (void**) &wifi_config_dct, WICED_TRUE, DCT_WIFI_CONFIG_SECTION, 0, sizeof( *wifi_config_dct ) );

    /* DEBUGGING */
    WPRINT_APP_INFO(("Property ID %d\r\n", property));

    switch (property)
    {

    /*===========================================================================*/
    /*                          PROPERTY = DEVICE_ID                             */
    /*===========================================================================*/
    case DEVICE_ID:

        /* Notes:
         *
         * BLE: CANNOT SET OR GET
         * Length = 6 bytes
         */

        /* DEBUGGING */
        WPRINT_APP_INFO(("Device id \r\n"));

        /* reject request if BLE */
        if (ble == interface_command_recieved_on)
        {
            WPRINT_APP_INFO((" BLE access not allowed \r\n"));
            /*Not allowed, return error*/
            send_response(SKYPLUG_ACCESS_DENIED, (uint8_t*)NULL, (int)NULL, property);

            break;
        }

        switch (current_action)
        {
        case set_default:

            /* Clear data */
            memset(app_dct->skyplug_device_id, 0, 6);

            /*  Save new value to dct */
            memcpy(app_dct->skyplug_device_id, DEFAULT_DEVICE_ID, 6);

            WPRINT_APP_INFO( ( "Device id SET TO == %s\r\n", app_dct->skyplug_device_id ) );

            /*  Save in dct  */
            result = wiced_dct_write( (const void*) &(app_dct->skyplug_device_id), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_device_id), sizeof(app_dct->skyplug_device_id) );

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case set_value:


            /*TODO: Add check to make sure parameter does not exceed allowable length  */
            if (parameter_length > 6)
            {
                /*  Invalid parameter length , return failure */
                send_response(SKYPLUG_MAX_PARAMETER_LENGTH_EXCEEDED, (uint8_t*)NULL, (int)NULL, property);

                break;
            }

            /* Clear data */
            memset(app_dct->skyplug_device_id, 0, 6);

            /*  Save new value to dct struct  */
            memcpy(app_dct->skyplug_device_id, parameters, parameter_length);

            /*  Debug  */
            WPRINT_APP_INFO( ( "Device id SET TO == %s\r\n", app_dct->skyplug_device_id ) );

            /*  Save in dct  */
            result = wiced_dct_write( (const void*) &(app_dct->skyplug_device_id), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_device_id), sizeof(app_dct->skyplug_device_id) );

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case get_value://ffffff0a0201

            /*  send response back */
            send_response(SKYPLUG_SUCCESS, (uint8_t *)&(app_dct->skyplug_device_id), sizeof(app_dct->skyplug_device_id), property);

            /*  DEBUGGING  */
            char test[6];
            memcpy(test, app_dct->skyplug_device_id, 6);
            //snprintf(test, 6, "%s", app_dct->skyplug_device_id);
            WPRINT_APP_INFO( ( "Value ASCII: %s\r\n", test) );

            break;

        default:

            break;
        }

        break;

    /*===========================================================================*/
    /*                          PROPERTY = FIRMWARE_VERSION                      */
    /*===========================================================================*/
    case FIRMWARE_VERSION:

        /* Notes:
         * BLE: CANNOT SET OR GET
         * Length = 2 bytes
         */
        
        /* reject request if BLE */
        if (ble == interface_command_recieved_on)
        {
            WPRINT_APP_INFO((" BLE access not allowed \r\n"));
            /*Not allowed, return error*/
            send_response(SKYPLUG_ACCESS_DENIED, (uint8_t*)NULL, (int)NULL, property);

            break;
        }

        WPRINT_APP_INFO(("Firmware version \r\n"));

        switch (current_action)
        {
        case set_default:


            WPRINT_APP_INFO(("Set to default: Firmware version \r\n"));

            /* Clear data */
            memset(app_dct->skyplug_firmware_version, 0, 4);

            /*  Save new value to dct */
            memcpy(app_dct->skyplug_firmware_version, DEFAULT_FIRMWARE_VERSION, 4);


            WPRINT_APP_INFO( ( "Firmware Version SET TO == %s\r\n", app_dct->skyplug_firmware_version ) );

            /*  Save in dct  */
            result = wiced_dct_write( (const void*) &(app_dct->skyplug_firmware_version), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_firmware_version), sizeof(app_dct->skyplug_firmware_version) );

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }
            break;

        case set_value:

            /*TODO: Add check to make sure parameter does not exceed allowable length  */
            if (parameter_length > 4)//two hex bytes = 4 ascii characters
            {
                /*  Invalid parameter length , return failure */
                send_response(SKYPLUG_MAX_PARAMETER_LENGTH_EXCEEDED, (uint8_t*)NULL, (int)NULL, property);

                break;
            }

            /* Clear data */
            memset(app_dct->skyplug_firmware_version, 0, 4);

            /*  Save new value to dct struct  */
            memcpy(app_dct->skyplug_firmware_version, parameters, parameter_length);

            /*  */
            WPRINT_APP_INFO( ( "Firmware Version SET TO == %s\r\n", app_dct->skyplug_firmware_version ) );

            /*  Save in dct  */
            result = wiced_dct_write( (const void*) &(app_dct->skyplug_firmware_version), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_firmware_version), sizeof(app_dct->skyplug_firmware_version) );

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case get_value:
            
            send_response(SKYPLUG_SUCCESS, (uint8_t *)&(app_dct->skyplug_firmware_version), sizeof(app_dct->skyplug_firmware_version), property);

            WPRINT_APP_INFO( ( "FIRMWARE_VERSION: %s\r\n", app_dct->skyplug_firmware_version ) );

            break;

        default:

            send_response(SKYPLUG_INVALID_ACTION, (uint8_t*)NULL, (int)NULL, property);

            break;
        }

        break;
    /*===========================================================================*/
    /*                          PROPERTY = API_VERISON                           */
    /*===========================================================================*/
    case API_VERSION:

        /* Notes:
         * BLE: CAN GET BUT NOT SET
         * Length = 2 bytes
         */

        WPRINT_APP_INFO(("Api Version \r\n"));

        switch (current_action)
        {
        case set_default:

            /* reject request if BLE */
            if (ble == interface_command_recieved_on)
            {
                WPRINT_APP_INFO((" BLE access not allowed \r\n"));
                /*Not allowed, return error*/
                send_response(SKYPLUG_ACCESS_DENIED, (uint8_t*)NULL, (int)NULL, property);

                break;
            }

            WPRINT_APP_INFO(("Set to default: API version \r\n"));

            /* Clear data */
            memset(app_dct->skyplug_api_version, 0, 4);

            /*  Save new value to dct */
            memcpy(app_dct->skyplug_api_version, DEFAULT_API_VERSION, 4);

            /*  */
            WPRINT_APP_INFO( ( "API Version SET TO == %s\r\n", app_dct->skyplug_api_version ) );

            /*  Save in dct  */
            result = wiced_dct_write( (const void*) &(app_dct->skyplug_api_version), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_api_version), sizeof(app_dct->skyplug_api_version) );

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case set_value:

            /* reject request if BLE */
            if (ble == interface_command_recieved_on)
            {
                WPRINT_APP_INFO((" BLE access not allowed \r\n"));
                /*Not allowed, return error*/
                send_response(SKYPLUG_ACCESS_DENIED, (uint8_t*)NULL, (int)NULL, property);

                break;
            }

            /* Clear data */
            memset(app_dct->skyplug_api_version, 0, 4);

            /*  Save new value to dct struct  */
            memcpy(app_dct->skyplug_api_version, parameters, parameter_length);

            /*  */
            WPRINT_APP_INFO( ( "API Version SET TO == %s\r\n", app_dct->skyplug_api_version ) );

            /*  Save in dct  */
            result = wiced_dct_write( (const void*) &(app_dct->skyplug_api_version), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_api_version), sizeof(app_dct->skyplug_api_version) );


            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case get_value:

            /*  send response back */
            send_response(SKYPLUG_SUCCESS, (uint8_t *)&(app_dct->skyplug_api_version), sizeof(app_dct->skyplug_api_version), property);
            WPRINT_APP_INFO( ( "API VERSION:  %s\r\n", app_dct->skyplug_api_version ) );

            break;

        default:

            send_response(SKYPLUG_INVALID_ACTION, (uint8_t*)NULL, (int)NULL, property);

            break;
        }

        break;

    /*===========================================================================*/
    /*                          PROPERTY = DEVICE_KEY                            */
    /*===========================================================================*/
    case DEVICE_KEY:

        /* Notes:
         * BLE: CANNOT GET OR SET
         * Length = 16 bytes
         */

        /* reject request if BLE */
        if (ble == interface_command_recieved_on)
        {
            WPRINT_APP_INFO((" BLE access not allowed \r\n"));
            /*Not allowed, return error*/
            send_response(SKYPLUG_ACCESS_DENIED, (uint8_t*)NULL, (int)NULL, property);
            break;
        }

        WPRINT_APP_INFO(("Device Key \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value:/* Test set value command: 040104test  */

            /*
             * TEST COMMANDS
             * 040104test
             *
             */

            /*TODO: Add check to make sure parameter does not exceed allowable length  */
            if (parameter_length > 32)//16 hex bytes = 32 ascii characters
            {
                /*  Invalid parameter length , return failure */
                send_response(SKYPLUG_MAX_PARAMETER_LENGTH_EXCEEDED, (uint8_t*)NULL, (int)NULL, property);

                break;
            }

            /* Clear data */
            memset(app_dct->skyplug_device_key, 0, 16);

            /*  Save new value to dct struct  */
            memcpy(app_dct->skyplug_device_key, parameters, parameter_length);

            /*  */
            WPRINT_APP_INFO( ( "Device Key SET TO == %s\r\n", app_dct->skyplug_device_key ) );

            /*  Save in dct  */
            result = wiced_dct_write( (const void*) &(app_dct->skyplug_device_key), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_device_key), sizeof(app_dct->skyplug_device_key) );

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case get_value:
            //DONT THINK WE SHOULD ALLOW THIS TO BE GOTTEN 
            send_response(SKYPLUG_ACCESS_DENIED, (uint8_t*)NULL, (int)NULL, property);
            //WPRINT_APP_INFO( ( "%s\r\n", app_dct->skyplug_device_key ) );
            break;

        default:

            break;
        }

        break;
        //wiced_network_register_link_callback;
    /*===========================================================================*/
    /*                          PROPERTY = MQTT_ENDPOINT                         */
    /*===========================================================================*/
    case MQTT_ENDPOINT:

        /* Notes:
         * BLE: CANNOT GET OR SET
         * Length = variable
         *
         */

        /* reject request if BLE */
        if (ble == interface_command_recieved_on)
        {
            WPRINT_APP_INFO((" BLE access not allowed \r\n"));
            /*Not allowed, return error*/
            send_response(SKYPLUG_ACCESS_DENIED, (uint8_t*)NULL, (int)NULL, property);
            break;
        }

        WPRINT_APP_INFO(("MQTT Endpoint \r\n"));

        switch (current_action)
        {
        case set_default:
                    /* Clear data */
            memset(app_dct->skyplug_mqtt_endpoint, 0, 256);

            /*  Save new value to dct struct  */
            memcpy(app_dct->skyplug_mqtt_endpoint, "ageeuhminb4ob.iot.us-east-2.amazonaws.com", 42);

            /*  */
            WPRINT_APP_INFO( ( "MQTT endpoint SET TO == %s\r\n", app_dct->skyplug_mqtt_endpoint ) );

            /*  Save in dct  */
            result = wiced_dct_write( (const void*) &(app_dct->skyplug_mqtt_endpoint), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_mqtt_endpoint), sizeof(app_dct->skyplug_mqtt_endpoint) );

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case set_value:

            /* Clear data */
            memset(app_dct->skyplug_mqtt_endpoint, 0, 256);

            /*  Save new value to dct struct  */
            memcpy(app_dct->skyplug_mqtt_endpoint, parameters, parameter_length);

            /*  */
            WPRINT_APP_INFO( ( "MQTT endpoint SET TO == %s\r\n", app_dct->skyplug_mqtt_endpoint ) );

            /*  Save in dct  */
            result = wiced_dct_write( (const void*) &(app_dct->skyplug_mqtt_endpoint), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_mqtt_endpoint), sizeof(app_dct->skyplug_mqtt_endpoint) );

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case get_value:

            WPRINT_APP_INFO( ( "Endpoint length %d\r\n", strlen(app_dct->skyplug_mqtt_endpoint) ) );
            /*  send response back */
            send_response(SKYPLUG_SUCCESS, (uint8_t *)&(app_dct->skyplug_mqtt_endpoint), strlen(app_dct->skyplug_mqtt_endpoint), property);

            WPRINT_APP_INFO( ( "%s\r\n",app_dct->skyplug_mqtt_endpoint ) );

            break;

        default:

            send_response(SKYPLUG_INVALID_ACTION, (uint8_t*)NULL, (int)NULL, property);

            break;
        }

        break;

    /*===========================================================================*/
    /*                          PROPERTY = MQTT_PEM_CERTIFICATE                  */
    /*===========================================================================*/
    case MQTT_PEM_CERTIFICATE:

        /* Notes:
         * BLE: CANNOT GET OR SET
         * Length = variable?
         */
        /* reject request if BLE */
        if (ble == interface_command_recieved_on)
        {
            WPRINT_APP_INFO((" BLE access not allowed \r\n"));
            /*Not allowed, return error*/
            send_response(SKYPLUG_ACCESS_DENIED, (uint8_t*)NULL, (int)NULL, property);
            break;
        }

        WPRINT_APP_INFO(("Pem Cert \r\n"));

        switch (current_action)
        {
        case set_default:

            send_response(SKYPLUG_INVALID_ACTION, (uint8_t*)NULL, (int)NULL, property);

            break;

        case set_value:

            //Write New Value
            //clear first with memset
            //need to verify certificate is correct length first

            memset(security_dct->certificate, 0, sizeof(security_dct->certificate));
            memcpy(security_dct->certificate, parameters, parameter_length);

            WPRINT_APP_INFO( ( "Recieved Cert = ") );//%c\r\n", security_dct.certificate ) );

            char *cert = &security_dct->certificate[0];
            for (index = 0; index < sizeof(security_dct->certificate); index++){
                WPRINT_APP_INFO( ( "%c", (char)*cert) );
                cert++;
            }

            /* Write the uploaded certificate and private key into security section of DCT */
            if ( security_dct->certificate[0] != '\0')// && security_dct.private_key[0] != '\0' )
            {
                result = wiced_dct_write( (const void*) security_dct, DCT_SECURITY_SECTION, 0, sizeof( security_dct ) );
                security_dct->certificate[0] = '\0';
                //security_dct.private_key[0] = '\0';
            }

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case get_value:

            send_response(SKYPLUG_ACCESS_DENIED, (uint8_t*)NULL, (int)NULL, property);
            //PROBABLY SHOULD NOT ALLOW THIS TO BE READ....
            break;

        default:

            break;
        }

        break;

    /*===========================================================================*/
    /*                          PROPERTY = MQTT_PUBLIC_KEY                       */
    /*===========================================================================*/
    case MQTT_PUBLIC_KEY://??Is this needed

        /* Notes:
         * BLE: CANNOT GET OR SET
         * Length = variable
         */
        /* reject request if BLE */
        if (ble == interface_command_recieved_on)
        {
            WPRINT_APP_INFO((" BLE access not allowed \r\n"));
            /*Not allowed, return error*/
            send_response(SKYPLUG_ACCESS_DENIED, (uint8_t*)NULL, (int)NULL, property);
            break;
        }

        WPRINT_APP_INFO(("Public Key \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value:

            /* NOT TESTED
             * TEST COMMANDS
             *
             *
             */

            break;

        case get_value:

            break;

        default:

            break;
        }

        break;

    /*===========================================================================*/
    /*                          PROPERTY = MQTT_PRIVATE_KEY                      */
    /*===========================================================================*/
    case MQTT_PRIVATE_KEY:

        /* Notes:
         * BLE: CANNOT GET OR SET
         * Length = variable
         */
        /* reject request if BLE */
        if (ble == interface_command_recieved_on)
        {
            WPRINT_APP_INFO((" BLE access not allowed \r\n"));
            /*Not allowed, return error*/
            send_response(SKYPLUG_ACCESS_DENIED, (uint8_t*)NULL, (int)NULL, property);
            break;
        }

        WPRINT_APP_INFO(("Private Key \r\n"));

        switch (current_action)
        {
        case set_default:

            send_response(SKYPLUG_INVALID_ACTION, (uint8_t*)NULL, (int)NULL, property);

            break;

        case set_value:

            result = WICED_ERROR;

            memset(security_dct->private_key, 0, sizeof(security_dct->private_key));
            memcpy(security_dct->private_key, parameters, parameter_length);

            WPRINT_APP_INFO( ( "Recieved key = ") );//%c\r\n", security_dct.certificate ) );

            char *cert = (char *)security_dct->private_key;
            for (index = 0; index < sizeof(security_dct->private_key); index++){
                WPRINT_APP_INFO( ( "%c", (char)*cert) );
                cert++;
            }

            /* Write the uploaded certificate and private key into security section of DCT */
            if ( security_dct->private_key[0] != '\0')// && security_dct.private_key[0] != '\0' )
            {
                result = wiced_dct_write( (const void*) security_dct, DCT_SECURITY_SECTION, 0, sizeof( security_dct ) );
                security_dct->private_key[0] = '\0';
                //security_dct.private_key[0] = '\0';
            }

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case get_value:

            //PROBABLY SHOULD NOT ALLOW THIS TO BE READ....
            break;

        default:

            break;
        }

        break;

    /*===========================================================================*/
    /*                          PROPERTY = WIFI_SSID                             */
    /*===========================================================================*/
    case WIFI_SSID:

        /* Notes:
         * BLE: CAN GET AND SET
         * Length = variable
         */

        WPRINT_APP_INFO(("WiFi SSID \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value:

            /*
             * TEST COMMANDS
             * ffffff00070109JBAK688
             *
             */

            /*TODO: Add check to verify ssid is valid */

            /* get the wi-fi config section for modifying, any memory allocation required would be done inside wiced_dct_read_lock() */
            //wiced_dct_read_lock( (void**) &wifi_config_dct, WICED_TRUE, DCT_WIFI_CONFIG_SECTION, 0, sizeof( *wifi_config_dct ) );
            wifi_config_dct->stored_ap_list[0].details.SSID.length = parameter_length;
            memset( wifi_config_dct->stored_ap_list[0].details.SSID.value, 0, sizeof(wifi_config_dct->stored_ap_list[0].details.SSID.value) );
            memcpy( (char*)wifi_config_dct->stored_ap_list[0].details.SSID.value, parameters, parameter_length);

            /* Write config */
            result = wiced_dct_write( (const void*) wifi_config_dct, DCT_WIFI_CONFIG_SECTION, 0, sizeof(platform_dct_wifi_config_t) );

            //wiced_dct_read_unlock( (void*) wifi_config_dct, WICED_TRUE );

            /*  send response back */
            if (WICED_SUCCESS == result){
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case get_value:

            
            //ffffff00020209
            /* This breaks encryption*/
            send_response(SKYPLUG_SUCCESS, (uint8_t *)wifi_config_dct->stored_ap_list[0].details.SSID.value, (int)wifi_config_dct->stored_ap_list[0].details.SSID.length, property);

            WPRINT_APP_INFO( ( "WIFI SSID: \r\n") );

            wiced_uart_transmit_bytes( STDIO_UART, wifi_config_dct->stored_ap_list[0].details.SSID.value, wifi_config_dct->stored_ap_list[0].details.SSID.length);
            //WPRINT_APP_INFO( ( "%s\r\n", app_dct->skyplug_wifi_ssid ) );
            break;

        default:

            break;
        }

        break;
    
    /*===========================================================================*/
    /*                          PROPERTY = WIFI_AUTH_TYPE                        */
    /*===========================================================================*/
    case WIFI_AUTH_TYPE:

        /* Notes:
         * BLE: CAN SET BUT CANNOT GET
         * Length = 1 byte
         */

        WPRINT_APP_INFO(("WiFi Auth Type \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value:

            /*
             * TEST COMMANDS
             * 07010AJBAK688
             *
             */

            //Write New Value
            //read this from wifi_config_dct not app_dct. Better to use existing??
            break;

        case get_value:

            /* reject request if BLE */
            if (ble == interface_command_recieved_on)
            {
                WPRINT_APP_INFO((" BLE access not allowed \r\n"));
                /*Not allowed, return error*/
                send_response(SKYPLUG_ACCESS_DENIED, (uint8_t*)NULL, (int)NULL, property);
                break;
            }


            //WPRINT_APP_INFO( ( "%s\r\n", app_dct->skyplug_wifi_auth_type ) );
            break;

        default:
            break;
        }

        break;

    /*===========================================================================*/
    /*                          PROPERTY = WIFI_USERNAME                         */
    /*===========================================================================*/
    case WIFI_USERNAME://what would this even be used for??

        /* Notes:
         * BLE: CAN SET BUT CANNOT GET
         * Length = variable
         */

        WPRINT_APP_INFO(("WiFi Username \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value:

            /* NOT TESTED
             * TEST COMMANDS
             *
             *
             */

            //Write New Value
            //memcpy(app_dct->skyplug_wifi_username, parameters, parameter_length);
            break;

        case get_value:

            /* reject request if BLE */
            if (ble == interface_command_recieved_on)
            {
                WPRINT_APP_INFO((" BLE access not allowed \r\n"));
                /*Not allowed, return error*/
                send_response(SKYPLUG_ACCESS_DENIED, (uint8_t*)NULL, (int)NULL, property);
                break;
            }
            //WPRINT_APP_INFO( ( "%s\r\n", app_dct->skyplug_wifi_username ) );
            break;

        default:
            break;
        }

        break;

    /*===========================================================================*/
    /*                          PROPERTY = WIFI_PASSWORD                         */
    /*===========================================================================*/
    case WIFI_PASSWORD:

        /* Notes:
         * BLE: CAN SET BUT CANNOT GET
         * Length = variable
         */

        WPRINT_APP_INFO(("Wifi Password \r\n"));

        switch (current_action)
        {
        case set_default:

            break;

        case set_value:

            /*
             * TEST COMMANDS
             * 0a010chelloworld
             *
             */

            /* get the wi-fi config section for modifying, any memory allocation required would be done inside wiced_dct_read_lock() */
            //wiced_dct_read_lock( (void**) &wifi_config_dct, WICED_TRUE, DCT_WIFI_CONFIG_SECTION, 0, sizeof( *wifi_config_dct ) );

            /* Save credentials for non-enterprise AP */
            memcpy((char*)wifi_config_dct->stored_ap_list[0].security_key, parameters, parameter_length);
            wifi_config_dct->stored_ap_list[0].security_key_length = parameter_length;

            /* Write config */
            result = wiced_dct_write( (const void*) wifi_config_dct, DCT_WIFI_CONFIG_SECTION, 0, sizeof(platform_dct_wifi_config_t) );


            /*  send response back */
            if (WICED_SUCCESS == result){
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }
            //wiced_dct_read_unlock( (void*) wifi_config_dct, WICED_TRUE );

            break;

        case get_value:

            /* reject request if BLE */
            if (ble == interface_command_recieved_on)
            {
                WPRINT_APP_INFO((" BLE access not allowed \r\n"));
                /*Not allowed, return error*/
                send_response(SKYPLUG_ACCESS_DENIED, (uint8_t*)NULL, (int)NULL, property);
                break;
            }

            //WPRINT_APP_INFO( ( "%s\r\n", app_dct->skyplug_wifi_password ) );
            break;

        default:

            break;
        }

        break;
    
    /*===========================================================================*/
    /*                          PROPERTY = SCANNED_SSIDS                         */
    /*===========================================================================*/
    case SCANNED_SSIDS:

        /* Notes:
         * BLE: CAN GET BUT CANNOT SET
         * Length = variable
         * They are proposing that results be separated by 0xffff
         */

        WPRINT_APP_INFO(("Scanned SSIDs \r\n"));

        switch (current_action)
        {
        case set_default:
        case set_value:

            /* reject request if BLE */
            if (ble == interface_command_recieved_on)
            {
                WPRINT_APP_INFO((" BLE access not allowed \r\n"));
                /*Not allowed, return error*/
                send_response(SKYPLUG_ACCESS_DENIED, (uint8_t*)NULL, (int)NULL, property);
                break;
            }
            /*
             * TEST COMMANDS
             *
             *
             */

            //Write New Value
            break;

        case get_value:

            send_response(SKYPLUG_INVALID_ACTION, (uint8_t*)NULL, (int)NULL, property);

            break;

        default:

            send_response(SKYPLUG_INVALID_ACTION, (uint8_t*)NULL, (int)NULL, property);

            break;
        }

        break;

    /*===========================================================================*/
    /*                          PROPERTY = HAS_FAN                               */
    /*===========================================================================*/
    case HAS_FAN:

        /* Notes:
         * BLE: CAN GET BUT CANNOT SET
         * Length = 1
         * ALLOWABLE VALUES: 0x00 = YES, 0x01 = NO
         */

        WPRINT_APP_INFO(("Has Fan \r\n"));

        switch (current_action)
        {
        case set_default:

            /* reject request if BLE */
            if (ble == interface_command_recieved_on)
            {
                WPRINT_APP_INFO((" BLE access not allowed \r\n"));
                /*Not allowed, return error*/
                send_response(SKYPLUG_ACCESS_DENIED, (uint8_t*)NULL, (int)NULL, property);
                break;
            }

            /* Clear data */
            memset(app_dct->skyplug_has_fan, 0, 2);

            /*  Save new value to dct struct  */
            memcpy(app_dct->skyplug_has_fan, DEFAULT_HAS_FAN, 2);

            /*  */
            WPRINT_APP_INFO( ( "Has fan set to: %s\r\n", app_dct->skyplug_has_fan ) );

            /*  Save in dct  */
            result = wiced_dct_write( (const void*) &(app_dct->skyplug_has_fan), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_has_fan), sizeof(app_dct->skyplug_has_fan) );

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case set_value:
            /* reject request if BLE */
            if (ble == interface_command_recieved_on)
            {
                WPRINT_APP_INFO((" BLE access not allowed \r\n"));
                /*Not allowed, return error*/
                send_response(SKYPLUG_ACCESS_DENIED, (uint8_t*)NULL, (int)NULL, property);
                break;
            }

            /* Clear data */
            memset(app_dct->skyplug_has_fan, 0, 2);

            /*  Save new value to dct struct  */
            memcpy(app_dct->skyplug_has_fan, parameters, parameter_length);

            /*  */
            WPRINT_APP_INFO( ( "Has fan set to: %s\r\n", app_dct->skyplug_has_fan ) );

            /*  Save in dct  */
            result = wiced_dct_write( (const void*) &(app_dct->skyplug_has_fan), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_has_fan), sizeof(app_dct->skyplug_has_fan) );

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case get_value:

            send_response(SKYPLUG_SUCCESS, app_dct->skyplug_has_fan, sizeof(app_dct->skyplug_has_fan), property);
            WPRINT_APP_INFO( ( "Has fan: %s\r\n", app_dct->skyplug_has_fan) );

            break;

        default:
                send_response(SKYPLUG_INVALID_ACTION, (uint8_t*)NULL, (int)NULL, property);
            break;
        }

        break;

    /*===========================================================================*/
    /*                          PROPERTY = FAN_SPEEDS                            */
    /*===========================================================================*/
    case FAN_SPEEDS:

        /* Notes:
         * BLE: CAN GET BUT CANNOT SET
         * Length = 1
         * ALLOWABLE VALUES: 0x05 = 5 SPEED, 0x07 = 7 SPEED, 0xFF = NO FAN
         */

        WPRINT_APP_INFO(("Fan speeds \r\n"));

        switch (current_action)
        {
        case set_default:

            /* reject request if BLE */
            if (ble == interface_command_recieved_on)
            {
                WPRINT_APP_INFO((" BLE access not allowed \r\n"));
                /*Not allowed, return error*/
                send_response(SKYPLUG_ACCESS_DENIED, (uint8_t*)NULL, (int)NULL, property);
                break;
            }


            /* Clear data */
            memset(app_dct->skyplug_fan_speeds, 0, 2);

            /*  Save new value to dct struct  */
            memcpy(app_dct->skyplug_fan_speeds, DEFAULT_FAN_SPEEDS, 2);

            /*  */
            WPRINT_APP_INFO( ( "Fan speeds : %s\r\n", app_dct->skyplug_fan_speeds ) );

            /*  Save in dct  */
            result = wiced_dct_write( (const void*) &(app_dct->skyplug_fan_speeds), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_fan_speeds), sizeof(app_dct->skyplug_fan_speeds) );

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case set_value:

            /* reject request if BLE */
            if (ble == interface_command_recieved_on)
            {
                WPRINT_APP_INFO((" BLE access not allowed \r\n"));
                /*Not allowed, return error*/
                send_response(SKYPLUG_ACCESS_DENIED, (uint8_t*)NULL, (int)NULL, property);
                break;
            }

            /* Clear data */
            memset(app_dct->skyplug_fan_speeds, 0, 2);

            /*  Save new value to dct struct  */
            memcpy(app_dct->skyplug_fan_speeds, parameters, parameter_length);

            /*  */
            WPRINT_APP_INFO( ( "Fan speeds: %s\r\n", app_dct->skyplug_fan_speeds ) );

            /*  Save in dct  */
            result = wiced_dct_write( (const void*) &(app_dct->skyplug_fan_speeds), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_fan_speeds), sizeof(app_dct->skyplug_fan_speeds) );

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case get_value:

            WPRINT_APP_INFO( ( "FAN SPEEDS:%s \r\n", app_dct->skyplug_fan_speeds) );
            send_response(SKYPLUG_SUCCESS, (uint8_t*)&app_dct->skyplug_fan_speeds, sizeof(app_dct->skyplug_fan_speeds), property);

            //WPRINT_APP_INFO( ( "%s\r\n", app_dct->skyplug_fan_speeds ) );
            break;

        default:
                send_response(SKYPLUG_INVALID_ACTION, (uint8_t*)NULL, (int)NULL, property);
            break;
        }

        break;

    /*===========================================================================*/
    /*                          PROPERTY = IS_FAN_REVERSIBLE                     */
    /*===========================================================================*/
    case IS_FAN_REVERSIBLE:

        /* Notes:
         * BLE: CAN GET BUT CANNOT SET
         * Length = 1
         * ALLOWABLE VALUES: 0x00 = NO, 0x01 = YES, 0xFF = NO FAN
         */

        WPRINT_APP_INFO(("Is fan reversible \r\n"));

        switch (current_action)
        {
        case set_default:
            /* reject request if BLE */
            if (ble == interface_command_recieved_on)
            {
                WPRINT_APP_INFO((" BLE access not allowed \r\n"));
                /*Not allowed, return error*/
                send_response(SKYPLUG_ACCESS_DENIED, (uint8_t*)NULL, (int)NULL, property);
                break;
            }

            /* Clear data */
            memset(app_dct->skyplug_is_fan_reversible, 0, 20);

            /*  Save new value to dct struct  */
            memcpy(app_dct->skyplug_is_fan_reversible, DEFAULT_IS_FAN_REVERSIBLE, 2);

            /*  */
            WPRINT_APP_INFO( ( "Is fan reversible == %s\r\n", app_dct->skyplug_is_fan_reversible ) );

            /*  Save in dct  */
            result = wiced_dct_write( (const void*) &(app_dct->skyplug_is_fan_reversible), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_is_fan_reversible), sizeof(app_dct->skyplug_is_fan_reversible) );

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case set_value:
            /* reject request if BLE */
            if (ble == interface_command_recieved_on)
            {
                WPRINT_APP_INFO((" BLE access not allowed \r\n"));
                /*Not allowed, return error*/
                send_response(SKYPLUG_ACCESS_DENIED, (uint8_t*)NULL, (int)NULL, property);
                break;
            }

            /* Clear data */
            memset(app_dct->skyplug_is_fan_reversible, 0, 20);

            /*  Save new value to dct struct  */
            memcpy(app_dct->skyplug_is_fan_reversible, parameters, parameter_length);

            /*  */
            WPRINT_APP_INFO( ( "Is fan reversible == %s\r\n", app_dct->skyplug_is_fan_reversible ) );

            /*  Save in dct  */
            result = wiced_dct_write( (const void*) &(app_dct->skyplug_is_fan_reversible), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_is_fan_reversible), sizeof(app_dct->skyplug_is_fan_reversible) );

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case get_value:



            WPRINT_APP_INFO( ( "IS FAN REVERSIBLE: %s\r\n", app_dct->skyplug_is_fan_reversible) );
            send_response(SKYPLUG_SUCCESS, (uint8_t*)&app_dct->skyplug_is_fan_reversible, sizeof(app_dct->skyplug_is_fan_reversible), property);

            break;

        default:

            send_response(SKYPLUG_INVALID_ACTION, (uint8_t*)NULL, (int)NULL, property);
            break;
        }

        break;

    /*===========================================================================*/
    /*                          PROPERTY = LIGHT_MIN_DIM_LEVEL                   */
    /*===========================================================================*/
    case LIGHT_MIN_DIM_LEVEL:

        /* Notes:
         * BLE: CAN GET AND SET
         * Length = 1
         * ALLOWABLE VALUES: 0x00-0x64 (0-100%)
         */

        WPRINT_APP_INFO(("Light min dim level \r\n"));

        switch (current_action)
        {
        case set_default:
            /* reject request if BLE */
            if (ble == interface_command_recieved_on)
            {
                WPRINT_APP_INFO((" BLE access not allowed \r\n"));
                /*Not allowed, return error*/
                send_response(SKYPLUG_ACCESS_DENIED, (uint8_t*)NULL, (int)NULL, property);
                break;
            }

                        /* Clear data */
            memset(app_dct->skyplug_light_min_dim_level, 0, 2);

            /*  Save new value to dct struct  */
            memcpy(app_dct->skyplug_light_min_dim_level, DEFAULT_LIGHT_MIN_DIM_LEVEL, 2);

            /*  */
            WPRINT_APP_INFO( ( "LIGHT MIN DIM LEVEL == %s\r\n", app_dct->skyplug_light_min_dim_level ) );

            /*  Save in dct  */
            result = wiced_dct_write( (const void*) &(app_dct->skyplug_light_min_dim_level), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_light_min_dim_level), sizeof(app_dct->skyplug_light_min_dim_level) );

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case set_value:
            /* reject request if BLE */
            if (ble == interface_command_recieved_on)
            {
                WPRINT_APP_INFO((" BLE access not allowed \r\n"));
                /*Not allowed, return error*/
                send_response(SKYPLUG_ACCESS_DENIED, (uint8_t*)NULL, (int)NULL, property);
                break;
            }

            /* Clear data */
            memset(app_dct->skyplug_light_min_dim_level, 0, 20);

            /*  Save new value to dct struct  */
            memcpy(app_dct->skyplug_light_min_dim_level, parameters, parameter_length);

            /*  */
            WPRINT_APP_INFO( ( "LIGHT MIN DIM LEVEL == %s\r\n", app_dct->skyplug_light_min_dim_level ) );

            /*  Save in dct  */
            result = wiced_dct_write( (const void*) &(app_dct->skyplug_light_min_dim_level), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_light_min_dim_level), sizeof(app_dct->skyplug_light_min_dim_level) );

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case get_value:

            WPRINT_APP_INFO( ( "LIGHT MIN DIM LEVEL %s\r\n",  app_dct->skyplug_light_min_dim_level ) );
            send_response(SKYPLUG_SUCCESS, (uint8_t*)&app_dct->skyplug_light_min_dim_level, sizeof(app_dct->skyplug_light_min_dim_level), property);

            break;

        default:
                send_response(SKYPLUG_INVALID_ACTION, (uint8_t*)NULL, (int)NULL, property);
            break;
        }

        break;

    /*===========================================================================*/
    /*                          PROPERTY = LIGHT_TRANSITION_TIME                 */
    /*===========================================================================*/
    case LIGHT_TRANSITION_TIME:

        /* Notes:<<<RESERVED FOR FUTURE USE>>>>>
         * BLE: CAN GET AND SET
         * Length = 1
         * ALLOWABLE VALUES: 0x00-0xFF
         */

        WPRINT_APP_INFO(("Light transition time \r\n"));

        switch (current_action)
        {
        case set_default:

            /* Clear data */
            memset(app_dct->skyplug_light_transition_time, 0, 2);

            /*  Save new value to dct struct  */
            memcpy(app_dct->skyplug_light_transition_time, DEFAULT_LIGHT_TRANSITION_TIME, 2);

            /*  */
            WPRINT_APP_INFO( ( "Light transition time: %s\r\n", app_dct->skyplug_light_transition_time ) );

            /*  Save in dct  */
            result = wiced_dct_write( (const void*) &(app_dct->skyplug_light_transition_time), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_light_transition_time), sizeof(app_dct->skyplug_light_transition_time) );

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case set_value:

            /* Clear data */
            memset(app_dct->skyplug_light_transition_time, 0, 20);

            /*  Save new value to dct struct  */
            memcpy(app_dct->skyplug_light_transition_time, parameters, parameter_length);

            /*  */
            WPRINT_APP_INFO( ( "Light transition time: %s\r\n", app_dct->skyplug_light_transition_time ) );

            /*  Save in dct  */
            result = wiced_dct_write( (const void*) &(app_dct->skyplug_light_transition_time), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_light_transition_time), sizeof(app_dct->skyplug_light_transition_time) );

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case get_value:

            WPRINT_APP_INFO( ( "Light transition time: %s\r\n", app_dct->skyplug_light_transition_time ) );
            send_response(SKYPLUG_SUCCESS, (uint8_t*)&app_dct->skyplug_light_transition_time, sizeof(app_dct->skyplug_light_transition_time), property);

            break;

        default:

            send_response(SKYPLUG_INVALID_ACTION, (uint8_t*)NULL, (int)NULL, property);

            break;
        }

        break;

    /*===========================================================================*/
    /*                          PROPERTY = IS_LIGHT_DIMMABLE                     */
    /*===========================================================================*/
    case IS_LIGHT_DIMMABLE:

        /* Notes:<<<RESERVED FOR FUTURE USE>>>>>
         * BLE: CAN GET BUT CANNOT SET
         * Length = 1
         * ALLOWABLE VALUES: 0x00-0xFF
         */

        WPRINT_APP_INFO(("Is light dimmable \r\n"));

        switch (current_action)
        {
        case set_default:
            /* reject request if BLE */
            if (ble == interface_command_recieved_on)
            {
                WPRINT_APP_INFO((" BLE access not allowed \r\n"));
                /*Not allowed, return error*/
                send_response(SKYPLUG_ACCESS_DENIED, (uint8_t*)NULL, (int)NULL, property);
                break;
            }

            /* Clear data */
            memset(app_dct->skyplug_is_light_dimmable, 0, 2);

            /*  Save new value to dct struct  */
            memcpy(app_dct->skyplug_is_light_dimmable, "00", 2);

            /*  */
            WPRINT_APP_INFO( ( "Is light dimmable: %s\r\n", app_dct->skyplug_is_light_dimmable ) );

            /*  Save in dct  */
            result = wiced_dct_write( (const void*) &(app_dct->skyplug_is_light_dimmable), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_is_light_dimmable), sizeof(app_dct->skyplug_is_light_dimmable) );

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case set_value:

            /* reject request if BLE */
            if (ble == interface_command_recieved_on)
            {
                WPRINT_APP_INFO((" BLE access not allowed \r\n"));
                /*Not allowed, return error*/
                send_response(SKYPLUG_ACCESS_DENIED, (uint8_t*)NULL, (int)NULL, property);
                break;
            }

            /* Clear data */
            memset(app_dct->skyplug_is_light_dimmable, 0, 2);

            /*  Save new value to dct struct  */
            memcpy(app_dct->skyplug_is_light_dimmable, parameters, parameter_length);

            /*  */
            WPRINT_APP_INFO( ( "Is light dimmable:  %s\r\n", app_dct->skyplug_is_light_dimmable ) );

            /*  Save in dct  */
            result = wiced_dct_write( (const void*) &(app_dct->skyplug_is_light_dimmable), DCT_APP_SECTION, OFFSETOF(skyplug_config_dct_t, skyplug_is_light_dimmable), sizeof(app_dct->skyplug_is_light_dimmable) );

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case get_value:

            send_response(SKYPLUG_SUCCESS, (uint8_t*)&app_dct->skyplug_is_light_dimmable, sizeof(app_dct->skyplug_is_light_dimmable), property);
            WPRINT_APP_INFO( ("Is light dimmable:  %s\r\n", app_dct->skyplug_is_light_dimmable) );

            break;

        default:

            send_response(SKYPLUG_INVALID_ACTION, (uint8_t*)NULL, (int)NULL, property);

            break;
        }

        break;

    /*===========================================================================*/
    /*                          PROPERTY = WIFI_STATUS                           */
    /*===========================================================================*/
    case WIFI_STATUS:

        /* Notes:
         * BLE,UART: CAN GET BUT NOT SET
         * Length = 1
         * ALLOWABLE VALUES: 0x00 = NO NETWORK SET, 0x01 = CONNECTED, 0x02 = NETWORK UNAVAILABLE
         * 0x03 = INVALID CREDENTIALS , 0x04 = CAN'T CONNECT TO MQTT SERVER , 0x05 = UNKNOWN ERROR , 0x06 = SCANNING
         *
         * DEFAULT VALUE = 0x00
         */

        WPRINT_APP_INFO(("WiFi Status \r\n"));

        switch (current_action)
        {
        case set_default:
        case set_value:
            
            send_response(SKYPLUG_INVALID_ACTION, (uint8_t*)NULL, (int)NULL, property);

            break;

        case get_value:

            send_response(SKYPLUG_SUCCESS, (uint8_t*)&network_status, 1, property);
            WPRINT_APP_INFO(("WiFi Status %d \r\n", network_status));

            break;

        default:
            
            send_response(SKYPLUG_INVALID_ACTION, (uint8_t*)NULL, (int)NULL, property);

            break;
        }

        break;

    /*===========================================================================*/
    /*                          PROPERTY = LIGHT_POWER                           */
    /*===========================================================================*/
    case LIGHT_POWER:

        /* Notes:
         * BLE: CAN GET AND SET
         * Length = 1 byte
         * ALLOWABLE VALUES: 0x00 = OFF, 0x01 = ON
         *
         * DEFAULT VALUE = 0x01
         */

        WPRINT_APP_INFO(("Light power \r\n"));

        switch (current_action)
        {
        case set_default:

            /*  Set light power in hardware  */
            result = skyplug_set_light_power(1);

            /*  If we were able to successfully set the hardware, save the value and update shadow */
            if (WICED_SUCCESS == result)
            {

                /* Save value */
                memcpy(skyplug_light_power, "01", 2);

                WPRINT_APP_INFO( ( "Light Power Set to: %s\r\n", skyplug_light_power) );

                /*  Update AWS shadow */
                /*  Need to update both desired and reported.  The new state is the desired state as well as the reported state*/
                update_aws_shadow_reported_and_desired();
            }

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case set_value:

            /* copy string representation of value */
            memcpy( command_value_string, parameters, 2);

            /* Get integer representation of value */
            command_value_int = new_hex_str_to_int(command_value_string);

            /*  Set light power */
            result = skyplug_set_light_power((uint8_t)command_value_int);

            /*  If we were able to successfully set the hardware, save the value and update shadow */
            if (WICED_SUCCESS == result)
            {

                /* Save value */
                memcpy(skyplug_light_power, parameters, 2);

                WPRINT_APP_INFO( ( "Light Power Set to: %s\r\n", skyplug_light_power) );

                /*  Update AWS shadow */
                /*  Need to update both desired and reported.  The new state is the desired state as well as the reported state*/
                update_aws_shadow_reported_and_desired();
            }

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case get_value:

            //ffffff02028101
            send_response(SKYPLUG_SUCCESS, (uint8_t *)&skyplug_light_power, strlen(skyplug_light_power), property);
            WPRINT_APP_INFO( ( "Stored Light Power %s\r\n", skyplug_light_power) );

            break;

        default:

            send_response(SKYPLUG_INVALID_ACTION, (uint8_t*)NULL, (int)NULL, property);

            break;
        }

        break;

    /*===========================================================================*/
    /*                          PROPERTY = LIGHT_LEVEL                           */
    /*===========================================================================*/
    case LIGHT_LEVEL:

        /* Notes:
         * BLE: CAN GET AND SET
         * Length = 1 byte
         * ALLOWABLE VALUES: 0x00- 0xFF
         *
         * DEFAULT VALUE = 0xff
         */

        WPRINT_APP_INFO(("Light Level \r\n"));

        switch (current_action)
        {
        case set_default:

            /*  Set light level in hardware */
            result = skyplug_set_light_level(255);

            /*  If we were able to successfully set the hardware, save the value and update shadow */
            if (WICED_SUCCESS == result)
            {
                /* Save value */
                memcpy(skyplug_light_level, "ff", 2);

                WPRINT_APP_INFO( ( "Light level: %s\r\n", skyplug_light_level) );
                /*  Update AWS shadow */
                /*  Need to update both desired and reported.  The new state is the desired state as well as the reported state*/
                update_aws_shadow_reported_and_desired();
            }

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case set_value:

            /* copy string representation of value */
            memcpy( command_value_string, parameters, 2);

            /* Get integer representation of value */
            command_value_int = new_hex_str_to_int(command_value_string);

            /*  Set light power in hardware */
            result = skyplug_set_light_level((uint8_t)command_value_int);

            /*  If we were able to successfully set the hardware, save the value and update shadow */
            if (WICED_SUCCESS == result)
            {

                /* Save value */
                memcpy(skyplug_light_level, parameters, 2);

                WPRINT_APP_INFO( ( "Light level: %s\r\n", skyplug_light_level) );
                /*  Update AWS shadow */
                /*  Need to update both desired and reported.  The new state is the desired state as well as the reported state*/
                update_aws_shadow_reported_and_desired();
            }

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case get_value:


            WPRINT_APP_INFO( ( "Light Level %s\r\n", skyplug_light_level ) );
            send_response(SKYPLUG_SUCCESS, (uint8_t*)&skyplug_light_level, sizeof(skyplug_light_level), property);
            break;

        default:

            send_response(SKYPLUG_INVALID_ACTION, (uint8_t*)NULL, (int)NULL, property);

            break;
        }

        break;

    /*===========================================================================*/
    /*                          PROPERTY = FAN_POWER                             */
    /*===========================================================================*/
    case FAN_POWER:

        /* Notes:
         * BLE: CAN GET AND SET
         * Length = 1 byte
         * ALLOWABLE VALUES: 0x00 = OFF, 0x01 = ON
         *
         * DEFAULT VALUE = 0x00
         */

        WPRINT_APP_INFO(("Fan power \r\n"));

        switch (current_action)
        {
        case set_default:

            /*  Set fan power */
            result = skyplug_set_fan_power(0);

            /*  If we were able to successfully set the hardware, save the value and update shadow */
            if (WICED_SUCCESS == result)
            {

                /* Save value */
                memcpy(skyplug_fan_power, "00", 2);

                WPRINT_APP_INFO( ( "Fan power SET TO: %s\r\n", skyplug_fan_power) );

                /*  Update AWS shadow */
                /*  Need to update both desired and reported.  The new state is the desired state as well as the reported state*/
                update_aws_shadow_reported_and_desired();
            }

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case set_value:

            /* copy string representation of value */
            memcpy( command_value_string, parameters, 2);

            /* Get integer representation of value */
            command_value_int = new_hex_str_to_int(command_value_string);

            /*  Set fan power */
            result = skyplug_set_fan_power((uint8_t)command_value_int);

            /*  If we were able to successfully set the hardware, save the value and update shadow */
            if (WICED_SUCCESS == result)
            {

                /* Save value */
                memcpy(skyplug_fan_power, parameters, 2);

                WPRINT_APP_INFO( ( "Fan power SET TO: %s\r\n", skyplug_light_level) );

                /*  Update AWS shadow */
                /*  Need to update both desired and reported.  The new state is the desired state as well as the reported state*/
                update_aws_shadow_reported_and_desired();
            }

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case get_value:

            WPRINT_APP_INFO( ( "Fan Power %s\r\n", skyplug_fan_power ) );
            send_response(SKYPLUG_SUCCESS, (uint8_t*)&skyplug_fan_power, sizeof(skyplug_fan_power), property);

            break;

        default:

            send_response(SKYPLUG_INVALID_ACTION, (uint8_t*)NULL, (int)NULL, property);

            break;
        }

        break;

    /*===========================================================================*/
    /*                          PROPERTY = FAN_SPEED                             */
    /*===========================================================================*/
    case FAN_SPEED:

        /* Notes:
         * BLE: CAN GET AND SET
         * Length = 1 byte
         * ALLOWABLE VALUES: 0x00 - FAN_SPEEDS(5 OR 7)
         *
         * DEFAULT VALUE = 0x00
         */

        WPRINT_APP_INFO(("Fan speed \r\n"));

        switch (current_action)
        {
        case set_default:

            /*  Set fan in hardware */
            result = skyplug_set_fan_level(0);

            /*  If we were able to successfully set the hardware, save the value and update shadow */
            if (WICED_SUCCESS == result)
            {
                /* Save value */
                memcpy(skyplug_fan_level, "00", 2);

                WPRINT_APP_INFO( ( "Set fan level to %s\r\n", skyplug_fan_level ) );

                /*  Update AWS shadow */
                /*  Need to update both desired and reported.  The new state is the desired state as well as the reported state*/
                update_aws_shadow_reported_and_desired();
            }
            
            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case set_value:

            /* copy string representation of value */
            memcpy( command_value_string, parameters, 2);

            WPRINT_APP_INFO( ( "Parameter value %s\r\n", command_value_string ) );

            /* Get integer representation of value */
            command_value_int = new_hex_str_to_int(command_value_string);

            /*  Set fan level in hardware */
            result = skyplug_set_fan_level((uint8_t)command_value_int);

            /*  If we were able to successfully set the hardware, save the value and update shadow */
            if (WICED_SUCCESS == result)
            {
                /* Save value */
                memcpy(skyplug_fan_level, parameters, 2);

                WPRINT_APP_INFO( ( "Set fan speed SET TO %s\r\n", skyplug_fan_level ) );
                /*  Update AWS shadow */
                /*  Need to update both desired and reported.  The new state is the desired state as well as the reported state*/
                update_aws_shadow_reported_and_desired();
            }

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case get_value:

            WPRINT_APP_INFO( ( "Fan Level %s\r\n", skyplug_fan_level ) );
            send_response(SKYPLUG_SUCCESS, (uint8_t*)&skyplug_fan_level, sizeof(skyplug_fan_level), property);

            break;

        default:

            send_response(SKYPLUG_INVALID_ACTION, (uint8_t*)NULL, (int)NULL, property);

            break;
        }

        break;

    /*===========================================================================*/
    /*                          PROPERTY = FAN_DIRECTION                         */
    /*===========================================================================*/
    case FAN_DIRECTION:

        /* Notes:
         * BLE: CAN GET AND SET
         * Length = 1 byte
         * ALLOWABLE VALUES: 0x00 = REVERSE, 0x01 = FORWARD
         *
         * DEFAULT VALUE = 0x01
         */

        WPRINT_APP_INFO(("Fan direction \r\n"));

        switch (current_action)
        {
        case set_default:


            /*  Set light power in hardware */
            result = skyplug_set_fan_direction(1);

            /*  If we were able to successfully set the hardware, save the value and update shadow */
            if (WICED_SUCCESS == result)
            {
                /* Save value */
                memcpy(skyplug_fan_direction, "01", 2);


                WPRINT_APP_INFO( ( "Set fan direction to %s\r\n", skyplug_fan_direction ) );

                /*  Update AWS shadow */
                /*  Need to update both desired and reported.  The new state is the desired state as well as the reported state*/
                update_aws_shadow_reported_and_desired();

            }
            
            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case set_value:

            /* copy string representation of value */
            memcpy( command_value_string, parameters, 2);

            /* Get integer representation of value */
            command_value_int = new_hex_str_to_int(command_value_string);

            /*  Set light power in hardware */
            result = skyplug_set_fan_direction((uint8_t)command_value_int);

            /*  If we were able to successfully set the hardware, save the value and update shadow */
            if (WICED_SUCCESS == result)
            {
                /* Save value */
                memcpy(skyplug_fan_direction, parameters, 2);


                WPRINT_APP_INFO( ( "Set fan direction to %s\r\n", skyplug_fan_direction ) );
                /*  Update AWS shadow */
                /*  Need to update both desired and reported.  The new state is the desired state as well as the reported state*/
                update_aws_shadow_reported_and_desired();

            }

            /*  send response back */
            if (WICED_SUCCESS == result)
            {
                send_response(SKYPLUG_SUCCESS, (uint8_t*)NULL, (int)NULL, property);
            }
            else
            {
                send_response(SKYPLUG_FAILURE, (uint8_t*)NULL, (int)NULL, property);
            }

            break;

        case get_value:

            WPRINT_APP_INFO( ( "Fan Direction %s\r\n", skyplug_fan_direction ) );
            send_response(SKYPLUG_SUCCESS, (uint8_t*)&skyplug_fan_direction, sizeof(skyplug_fan_direction), property);
        
            break;

        default:

            send_response(SKYPLUG_INVALID_ACTION, (uint8_t*)NULL, (int)NULL, property);

            break;
        }

        break;

    default:

        WPRINT_APP_INFO(("Invalid Property \r\n"));
        send_response(SKYPLUG_INVALID_PROPERTY_OR_STATUS_ID, (uint8_t*)NULL, (int)NULL, property);

        //unknown property or status value
        break;
    }

    /* Release the read lock */
    wiced_dct_read_unlock( (void*) wifi_config_dct, WICED_TRUE );
    wiced_dct_read_unlock( (void*) app_dct, WICED_TRUE );
    wiced_dct_read_unlock( (void*) security_dct, WICED_TRUE );
}


/*
 ******************************************************************************
 * Send response after command received
 *
 * @param[in] command  The character to be inserted.
 *
 * @return    void
 */
/*TODO:  This needs to be refactored */
wiced_result_t send_response(int status, uint8_t *value, int value_length, int property)//interface_t interface)
{
    int command_length;
    uint16_t checksum;
    int index;

    //char *testResponse = "040073test0809";

    switch (current_action)
    {
    case set_value:
    case set_default:

        /* Set the command length */
        command_length = 1/*Status*/ + 2/*Checksum*/;

        uint8_t *response_data;
        response_data = (uint8_t *)malloc(command_length + 2/*CheckSum*/ + 3/*start sequence*/);
        uint8_t *response_data_head = response_data;
        uint8_t *response_data_p = response_data;

        /* add response start sequence if interface == uart*/
        if (uart == interface_command_recieved_on)
        {
            *response_data = (uint8_t)0xff;
            response_data++;

            *response_data = (uint8_t)0xff;
            response_data++;

            *response_data = (uint8_t)0xff;
            response_data++;
        }

        response_data_p = response_data;
        /* add length */
        *response_data = command_length;
        response_data++;

        /*  add status */
        *response_data = status;
        response_data++;

        /* Calculate checksum */
        checksum = Fletcher16(response_data_p, 2);

        /* Populate checksum bytes into response data  */
        *response_data = (checksum >> 8) & 0x00ff;
        response_data++;

        *response_data = (checksum & 0x00ff);

        WPRINT_APP_INFO( ( "RESPONSE \r\n") );
        /*DEBUGGGING --> Print Full Response */
        for (index = 0; index < command_length + 1/*length*/ + 3/*start sequence*/; index++)
        {
            WPRINT_APP_INFO( ( "%02x", response_data_head[index]) );
        }
        WPRINT_APP_INFO( ( "\r\n") );

                /* TESTING   */
        //if (ble == interface_command_recieved_on){
            process_bt_response(response_data_head, command_length + 1/*length*/ + 3/*start sequence*/);
        //}

        break;

    case get_value:

        /* Set the command length */
        command_length = 1/*status*/ + 1/*property*/+ value_length;

        uint8_t *get_response_data;
        get_response_data = (uint8_t *)malloc(command_length + 1/*length byte*/ + 2/*CheckSum*/ + 3/*start sequence*/);
        uint8_t *get_response_data_head = get_response_data;
        uint8_t *get_response_data_p = get_response_data;

        /* add response start sequence if interface == uart*/
        if (uart == interface_command_recieved_on)
        {
            *get_response_data = (uint8_t)0xff;
            get_response_data++;

            *get_response_data = (uint8_t)0xff;
            get_response_data++;

            *get_response_data = (uint8_t)0xff;
            get_response_data++;
        }

        get_response_data_p = get_response_data;

        /* add length */
        *get_response_data = (uint8_t)(command_length);
        get_response_data++;

        /* add status */
        *get_response_data = (uint8_t)status;//0x00;/*success*/
        get_response_data++;

        /* add property */
        *get_response_data = (uint8_t)property;
        get_response_data++;

        for (index = 0; index < value_length; index++)
        {
            *get_response_data = (uint8_t)*value;
            get_response_data++;
            value++;
        }

        /* Calculate checksum */
        checksum = Fletcher16(get_response_data_p, 1/*command length*/ + 1/*status*/ + 1/*property*/ + value_length);

        /* Populate checksum bytes into response data  */
        *get_response_data = (checksum >> 8) & 0x00ff;
        get_response_data++;

        *get_response_data = (checksum & 0x00ff);

        WPRINT_APP_INFO( ( "RESPONSE \r\n") );
        /*DEBUGGGING --> Print Full Response */
        for (index = 0; index < 1/*command length*/ + 1/*status*/ + 1/*property*/ + value_length + 2/*CheckSum*/ + 3/*start sequence*/; index++)
        {
           WPRINT_APP_INFO( ( "%02x", get_response_data_head[index]) );
        }
        WPRINT_APP_INFO( ( "\r\n") );

        /* TESTING   */
        //if (ble == interface_command_recieved_on){
        process_bt_response(get_response_data_head, 1/*command length*/ + 1/*status*/ + 1/*property*/ + value_length + 2/*CheckSum*/ + 3);
        //}

        //send_bt_command_response( get_response_data_head, 1/*command length*/ + 1/*status*/ + 1/*property*/ + value_length + 2/*CheckSum*/ + 3);

        break;

    default:

        break;
    }

    //wiced_uart_transmit_bytes( STDIO_UART, testResponse, sizeof(testResponse) );

    return WICED_SUCCESS;
}


void process_bt_response(uint8_t *unprocessed_response_data, uint8_t unprocessed_response_data_length)
{
    uint8_t encrypted_packet[16];
    uint8_t chunked_packet[18];
    uint8_t final_packet_length;

    int chunk_index = 1;

    uint8_t *processing_position = unprocessed_response_data;
    uint8_t start_length = unprocessed_response_data_length;

    #ifdef DEBUG_BT
    WPRINT_APP_INFO( ( "Unprocessed data length : %d \r\n", unprocessed_response_data_length) );
    #endif

    uint8_t number_of_chunks = (unprocessed_response_data_length/16) + 1;
    
    #ifdef DEBUG_BT
    WPRINT_APP_INFO( ( "Number of chunks: %d \r\n", number_of_chunks) );
    #endif 

    final_packet_length = unprocessed_response_data_length - ((number_of_chunks - 1) * 16);

    #ifdef DEBUG_BT
    WPRINT_APP_INFO( ( "Final packet length : %d \r\n", final_packet_length) );
    #endif

    /* if packet length is greater than 16, need to separate into chuncks*/
    if (unprocessed_response_data_length > 16)
    {
        for (chunk_index = 1; chunk_index <= number_of_chunks; chunk_index++)
        {
            /* final chunk */
            if (chunk_index == number_of_chunks)
            {
                run_encryption(processing_position, final_packet_length, encrypted_packet);
                
                chunked_packet[0] = number_of_chunks;
                chunked_packet[1] = chunk_index;

                for (int i = 2; i < 18; i++)
                {
                    chunked_packet[i] = encrypted_packet[i - 2];
                }

                send_bt_command_response( chunked_packet, 18);
            }
            else /* inidividual chunks */
            {
                run_encryption(processing_position, 16, encrypted_packet);

                chunked_packet[0] = number_of_chunks;
                chunked_packet[1] = chunk_index;

                for (int i = 2; i < 18; i++)
                {
                    chunked_packet[i] = encrypted_packet[i - 2];
                }

                send_bt_command_response( chunked_packet, 18);

                processing_position += 16;
            }   
        }
    }
    else /* only need to send one chunk */
    {
        run_encryption(unprocessed_response_data, unprocessed_response_data_length, encrypted_packet);

        chunked_packet[0] = 0x01;
        chunked_packet[1] = 0x01;

        for (int i = 2; i < 18; i++)
        {
            chunked_packet[i] = encrypted_packet[i - 2];
        }

        /* TODO: need to configure chunking based on data length */
        //send_bt_command_response( encrypted_packet, 16);
        send_bt_command_response( chunked_packet, 18);
    }


}


/*
 ******************************************************************************
 * Calculate checksum
 *
 * @param[in] command  The character to be inserted.
 *
 * @return    void
 */
uint16_t Fletcher16( uint8_t *data, int count )
{
   uint16_t final_checksum = 0;
   uint16_t sum1 = 0;
   uint16_t sum2 = 0;
   int index;

   for( index = 0; index < count; ++index )
   {
      sum1 = (sum1 + data[index]) % 255;
      //WPRINT_APP_INFO( ( "SUM1:  0x%04x", sum1) );
      sum2 = (sum2 + sum1) % 255;
      //WPRINT_APP_INFO( ( "  SUM2:  0x%04x\r\n", sum2) );
   }
   final_checksum = (sum2 << 8) | sum1;
   //WPRINT_APP_INFO( ( "\rChecksum :  0x%04x\r\n", final_checksum) );


   return (sum2 << 8) | sum1;
}

/*
 ******************************************************************************
 * Insert a character into the current line at the cursor position and update the screen.
 *
 * @param[in] c  The character to be inserted.
 *
 * @return    void
 */
void console_insert_char( char c )
{
    uint32_t i;
    uint32_t len = strlen( console_buffer );

    /* move the end of the line out to make space */
    for ( i = len + 1; i > console_cursor_position; i-- )
    {
        console_buffer[i] = console_buffer[i - 1];
    }

    /* insert the character */
    len++;
    console_buffer[console_cursor_position] = c;

    /* WASN'T WORKING UNLESS I ADDED THIS DELAY */

    //wiced_rtos_delay_milliseconds(2);
    //WPRINT_APP_INFO(("Current command %s\r\n", console_buffer));


    /* print out the modified part of the ConsoleBuffer */
    //send_str( &cons.console_buffer[cons.console_cursor_position] );

    /* move the cursor back to where it's supposed to be */
    console_cursor_position++;
    /*for ( i = len; i > console_cursor_position; i-- )
    {
        //send_char( '\b' );
    }*/
}

/*!
 ******************************************************************************
 * Convert a hexidecimal string to an integer.
 *
 * @param[in] hex_str  The string containing the hex value.
 *
 * @return    The value represented by the string.
 */

int new_hex_str_to_int( const char* hex_str )
{
    int n = 0;
    uint32_t value = 0;
    int shift = 7;
    while ( hex_str[n] != '\0' && n < 8 )
    {
        if ( hex_str[n] > 0x21 && hex_str[n] < 0x40 )
        {
            value |= ( hex_str[n] & 0x0f ) << ( shift << 2 );
        }
        else if ( ( hex_str[n] >= 'a' && hex_str[n] <= 'f' ) || ( hex_str[n] >= 'A' && hex_str[n] <= 'F' ) )
        {
            value |= ( ( hex_str[n] & 0x0f ) + 9 ) << ( shift << 2 );
        }
        else
        {
            break;
        }
        n++;
        shift--;
    }

    return ( value >> ( ( shift + 1 ) << 2 ) );
}

//Certificate includes new line characters, so we need to ignore new line and line feed characters
//and treat them as characters
/*
 *  Still need to figure out how to best process certificate
 */
void check_for_set_cert_command( interface_t interface )
{
    //should probably use a mutex or semaphore to lock command processing from addtional commands before processing addtional
    char command_value_string[2];
    int  command_value_int;

    char *command_ptr = console_buffer;
    interface_command_recieved_on = interface; /* save the interface we are processing the command from */
    //wiced_bool_t error = WICED_FALSE;

    /*  first two characters = first byte of command = parameter length */
    memcpy( command_value_string, console_buffer, 2);

    /* Get command length */
    parameter_length = new_hex_str_to_int(command_value_string);

    WPRINT_APP_INFO(("Param Length %d\r\n", parameter_length ));

    /*  Increment pointer to next byte in character array */
    command_ptr++;
    command_ptr++;

    /* second two characters = second byte of command = command verb */
    memcpy( command_value_string, command_ptr, 2);
    command_value_int = new_hex_str_to_int(command_value_string);

    /*  Increment pointer to next byte in character array */
    command_ptr++;
    command_ptr++;

    switch (command_value_int)
    {
    case SET_TO_DEFAULT:

        WPRINT_APP_INFO(("Set to defualt command\r\n"));
        current_action = set_default;
        break;

    case SET_VALUE:

        WPRINT_APP_INFO(("Set value command \r\n"));
        current_action = set_value;

        //Process action for property
        memcpy(command_value_string, command_ptr, 2);
        command_value_int = new_hex_str_to_int(command_value_string);

        if (MQTT_PEM_CERTIFICATE == command_value_int){

            WPRINT_APP_INFO(("Prepare to recieve certificate, ignore new line and line feed \r\n"));
            receiving_cert = TRUE;
        }

        break;

    case GET_VALUE:

        WPRINT_APP_INFO(("Get value \r\n"));
        current_action = get_value;

        break;

    default:
        break;
    }

    //Process action for property
    memcpy(command_value_string, command_ptr, 2);
    command_value_int = new_hex_str_to_int(command_value_string);

    command_ptr++;
    command_ptr++;
}




//BONEYARD
//            value_ptr = (uint8_t *)&(app_dct->skyplug_device_id);
//            response_command_length = 3/*ffffff*/ + 1/*length byte*/ + 1/*action*/ + 1/*property*/+ strlen(app_dct->skyplug_device_id)/*value*/;
//
//            //char ascii[response_command_length+2];
//            uint8_t *response_data;
//            response_data = (uint8_t *)malloc(response_command_length);
//
//            uint8_t *response_data_p = response_data;
//
//            *response_data = (uint8_t)0xff;
//            WPRINT_APP_INFO( ( "Response Data %02x\r\n", *response_data) );
//            response_data++;
//            *response_data = (uint8_t)0xff;
//            response_data++;
//            *response_data = (uint8_t)0xff;
//            response_data++;
//            *response_data = (uint8_t)(2 + strlen(app_dct->skyplug_device_id));
//            response_data++;
//            *response_data = (uint8_t)0x00;/*success*/
//            response_data++;
//            *response_data = (uint8_t)property;
//            response_data++;
//
//            for (index = 0; index < strlen(app_dct->skyplug_device_id); index++)
//            {
//                *response_data = (uint8_t)*value_ptr;
//                response_data++;
//                value_ptr++;
//            }
//

            //ffffff0b00014445564943455f49448d00
            //ffffff4400014445564943455f49443c00
//            /*DEBUGGGING*/
////            for (index = 0; index < response_command_length; index++)
////            {
////                WPRINT_APP_INFO( ( "%02x\r\n", response_data_p[index]) );
////
////            }
//
//            checksum = Fletcher16(response_data_p, response_command_length);
//            *response_data = (checksum >> 8)&0x00ff;
//            response_data++;
//            *response_data = (checksum&&0x00ff)>>8;
//
//            /*DEBUGGGING*/
//            for (index = 0; index < response_command_length + 2; index++)
//            {
//                WPRINT_APP_INFO( ( "%02x", response_data_p[index]) );
//            }
//            WPRINT_APP_INFO( ( "\r\n") );
            /* Print Device ID */

            //memcpy(ascii, response_data_p, (response_command_length + 2));

            //WPRINT_APP_INFO( ( "ASCII Response: %s\r\n", ascii  ) );
