
#include <stdlib.h>
#include <string.h>
#include "wiced.h"

#include <wiced_utilities.h>
#include <resources.h>
#include "wiced_framework.h"
#include "skyplug_lighting_interface.h"



/*
 *  Set the fan level to value in range {0x00, 0x06}
 */
wiced_result_t skyplug_set_fan_level(uint8_t fan_level){
    return WICED_SUCCESS;
}

/*
 *  Set fan power to either on 0x01 or off 0x00
 */
wiced_result_t skyplug_set_fan_power(uint8_t fan_power){
    return WICED_SUCCESS;
}

/*
 *  Set the fan direction to either forward 0x00 or reverse 0x01
 */
wiced_result_t skyplug_set_fan_direction(uint8_t fan_direction){
    return WICED_SUCCESS;
}
